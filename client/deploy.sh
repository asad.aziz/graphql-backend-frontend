echo "Starting deployment to dev"
git pull
DOCKER_REGISTRY=796550060731.dkr.ecr.eu-west-1.amazonaws.com
DOCKER_HOST='100.83.123.66:2376'
DOCKER_IMAGE=xfoil-frontend-v2
DOCKER_CONTAINER=frontend_app-v2
DOCKER_TAG=$(git branch --show-current)'-'$(git rev-parse --verify --short HEAD)

aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin $DOCKER_REGISTRY

echo "Rename template files" 
rm -f .env.production
cp .env.development.template .env.production
echo "Building docker image with tag="$DOCKER_TAG 
docker build -t $DOCKER_IMAGE:$DOCKER_TAG .
echo "Tagging and pushing to registry"
docker tag $DOCKER_IMAGE:$DOCKER_TAG $DOCKER_REGISTRY/$DOCKER_IMAGE:$DOCKER_TAG
docker push $DOCKER_REGISTRY/$DOCKER_IMAGE:$DOCKER_TAG
echo "Remove env files" 
rm -f .env.production

echo "Pulling image to dev box"
docker -H $DOCKER_HOST pull $DOCKER_REGISTRY/$DOCKER_IMAGE:$DOCKER_TAG

echo "Removing old container "$DOCKER_CONTAINER
docker -H $DOCKER_HOST rm -f $DOCKER_CONTAINER

echo "Creating new container "$DOCKER_CONTAINER
docker -H $DOCKER_HOST run -d --name $DOCKER_CONTAINER -v /node_modules -p 3000:3000 -e CHOKIDAR_USEPOLLING=true -e REACT_APP_BACKEND_BASE_URL=100.83.123.66:2376 $DOCKER_REGISTRY/$DOCKER_IMAGE:$DOCKER_TAG
sleep 10
docker -H $DOCKER_HOST ps -a

echo "Deployment completed successfully to dev"

if [[ $DOCKER_TAG == *"master"* ]]; then
	DOCKER_TAG_PROD=$DOCKER_TAG"-prod"
	echo "Rename template files" 
	rm -f .env.production
	cp .env.production.template .env.production
	echo "Building docker image with tag="$DOCKER_TAG_PROD 
	docker build -t $DOCKER_IMAGE:$DOCKER_TAG_PROD .
	echo "Tagging and pushing to registry"
	docker tag $DOCKER_IMAGE:$DOCKER_TAG_PROD $DOCKER_REGISTRY/$DOCKER_IMAGE:$DOCKER_TAG_PROD
	docker push $DOCKER_REGISTRY/$DOCKER_IMAGE:$DOCKER_TAG_PROD
	echo "Remove env files" 
	rm -f .env.production
fi

echo "Performing cleanup"
docker system prune -a -f --volumes
docker -H $DOCKER_HOST system prune -a -f --volumes
echo "Cleanup done successfully"
