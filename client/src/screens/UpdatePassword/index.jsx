import React, { useEffect, useState } from "react";
import { Form, Input, Button, Typography } from "antd";
import { withRouter } from "react-router";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  updatePassword,
  selectUpdatePassword,
} from "../../redux/updatePasswordSlice";
import { clearAllStates } from "../../redux/logoutSlice";
import Countdown from "react-countdown";

import loginScreenStyle from "./style.module.css";

const { Title } = Typography;

const UpdatePassword = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const updatePasswordStatus = useSelector(selectUpdatePassword);
  const [emptyFieldsError, setEmptyFieldsError] = useState(false);
  const [isPasswordUpdate, setIsPasswordUpdate] = useState(false);
  const [isPasswordSame, setIsPasswordSame] = useState(false);
  const [isPasswordUpdateFailed, setIsPasswordUpdateFailed] = useState(false);
  const [isBtnDisabled, setIsBtnDisabled] = useState(false);
  const onFinish = (values) => {
    setIsPasswordSame(false);
    setIsPasswordUpdate(false);
    setIsPasswordUpdateFailed(false);
    if (
      values.currentPassword === "" ||
      values.newPassword === "" ||
      values.currentPassword === undefined ||
      values.newPassword === undefined
    ) {
      setEmptyFieldsError(true);
    } else if (values.currentPassword === values.newPassword) {
      setIsPasswordSame(true);
    } else {
      setEmptyFieldsError(false);
      setIsBtnDisabled(true);
      dispatch(
        updatePassword({
          currentPassword: values.currentPassword,
          newPassword: values.newPassword,
        })
      );
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {
    if (updatePasswordStatus && updatePasswordStatus.isUpdated) {
      dispatch(clearAllStates());
      setIsPasswordUpdate(true);
      setIsBtnDisabled(false);
      setTimeout(() => {
        dispatch(clearAllStates());
        localStorage.clear();
        history.push("/login");
      }, 5000);
    } else if (
      updatePasswordStatus &&
      updatePasswordStatus.isUpdated === false
    ) {
      setIsBtnDisabled(false);
      dispatch(clearAllStates());
      setIsPasswordUpdateFailed(true);
    }
  }, [updatePasswordStatus, history]);

  const renderer = ({ hours, minutes, seconds, completed }) => {
    if (completed) {
      // Render a completed state
      return null;
    } else {
      // Render a countdown
      return <span>{seconds}</span>;
    }
  };

  return (
    <div className={loginScreenStyle.mainContainerUpdatePassword}>
      <Title level={2} className={loginScreenStyle.updatePasswordHeading}>
        Update Password
      </Title>
      <Form
        name="basic"
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
      >
        <Form.Item
          name="currentPassword"
          rules={[
            {
              required: true,
              message: "current password is required!",
            },
          ]}
        >
          <Input.Password placeholder="current password" />
        </Form.Item>

        <Form.Item
          name="newPassword"
          rules={[
            {
              required: true,
              message: "new password id required!",
            },
          ]}
          hasFeedback
        >
          <Input.Password placeholder="new password" />
        </Form.Item>

        <Form.Item
          name="confirmPassword"
          dependencies={["newPassword"]}
          hasFeedback
          rules={[
            {
              required: true,
              message: "Please confirm your password!",
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue("newPassword") === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error("passwords that you entered do not match!")
                );
              },
            }),
          ]}
        >
          <Input.Password placeholder="confirm password" />
        </Form.Item>

        <Form.Item>
          <Button
            type="primary"
            htmlType="submit"
            className={loginScreenStyle.UpdatePswFormButton}
            disabled={isBtnDisabled}
          >
            Update password
          </Button>
        </Form.Item>

        {emptyFieldsError ? (
          <p
            style={{
              textAlign: "center",
              paddingTop: "5px",
              paddingBottom: "5px",
              color: "red",
              fontSize: "13px",
            }}
          >
            Please enter both your current and new password!
          </p>
        ) : null}

        {/* {incorrectCredentials ? (
            <p
              style={{
                textAlign: "center",
                paddingTop: "5px",
                paddingBottom: "5px",
                color: "red",
                fontSize: "13px",
              }}
            >
              You email or password might be incorrect.
            </p>
          ) : null} */}
        {isPasswordSame ? (
          <p
            style={{
              textAlign: "center",
              paddingTop: "5px",
              paddingBottom: "5px",
              color: "red",
              fontSize: "13px",
            }}
          >
            Your current and new password is same.
          </p>
        ) : null}

        {isPasswordUpdate ? (
          <p
            style={{
              textAlign: "center",
              paddingTop: "5px",
              paddingBottom: "5px",
              color: "green",
              fontSize: "13px",
            }}
          >
            Password Updated Succesfully. Redirecting to login in{" "}
            <Countdown
              date={Date.now() + 5000}
              intervalDelay={0}
              precision={0}
              renderer={renderer}
            />
          </p>
        ) : null}
        {isPasswordUpdateFailed ? (
          <p
            style={{
              textAlign: "center",
              paddingTop: "5px",
              paddingBottom: "5px",
              color: "red",
              fontSize: "13px",
            }}
          >
            Error in updating password. Please try again
          </p>
        ) : null}
      </Form>
    </div>
  );
  // }
};

export default withRouter(UpdatePassword);
