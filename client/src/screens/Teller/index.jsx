import React from "react";
import { Row, Col, Typography } from "antd";
import tellerStyle from "./style.module.css";

import TotalCollectionCard from "../../components/Cards/TotalCollection";
import CurrentlyDueCard from "../../components/Cards/CurrentlyDue";
import CashInMachineCard from "../../components/Cards/CashInMachine";
import ActiveRidersCard from "../../components/Cards/ActiveRiders";
import CashDenoInHand from "../../components/Cards/CashDenoInHand";
import AddManualRecon from "../../components/ManualReconUpdateBox/AddManualRecon";
import HistoricData from "../../components/Subscreens/HistoricData";

const { Title } = Typography;

const Teller = () => {
  return (
    <>
      <Row>
        <Col xs={0} sm={0} md={10} lg={10} xl={10}></Col>
        <Col
          xs={24}
          sm={24}
          md={14}
          lg={14}
          xl={14}
          className={tellerStyle.topCol}
        ></Col>
      </Row>
      <Row>
        <Col xs={24} sm={24} md={14} lg={14} xl={14}>
          <Title level={4}>Today</Title>
        </Col>

        <Col xs={24} sm={24} md={10} lg={10} xl={10}></Col>
      </Row>

      <Row className={tellerStyle.mainCotainerCards}>
        <Col xs={24} sm={12} md={6} lg={6} xl={6}>
          <TotalCollectionCard />
        </Col>
        <Col xs={24} sm={12} md={6} lg={6} xl={6}>
          <CurrentlyDueCard />
        </Col>
        <Col xs={24} sm={12} md={6} lg={6} xl={6}>
          <CashInMachineCard />
        </Col>
        <Col xs={24} sm={12} md={6} lg={6} xl={6}>
          <ActiveRidersCard />
        </Col>
      </Row>
      <p className={tellerStyle.subContainerCardDenoHeading}>
        Your Denominations in Hand
      </p>
      <Row className={tellerStyle.denoContainer}>
        <CashDenoInHand />
      </Row>
      <Row className={tellerStyle.AddManualCashDenoRow}>
        <Col xs={24} sm={12} md={12} lg={12} xl={12}>
          <AddManualRecon />
        </Col>
        <Col xs={24} sm={12} md={12} lg={12} xl={12}>
          <HistoricData />
        </Col>
      </Row>
    </>
  );
};

export default Teller;
