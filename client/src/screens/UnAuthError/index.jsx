import React from "react";
import { Result, Button } from "antd";
import { useHistory } from "react-router-dom";

const UnauthError = () => {
  const history = useHistory();
  const backToLogin = () => {
    history.push("/login");
  };
  return (
    <Result
      status="403"
      title="403"
      subTitle="Sorry, your session has been expired."
      extra={
        <Button type="primary" onClick={backToLogin}>
          Please Login Again
        </Button>
      }
    />
  );
};

export default UnauthError;
