import React, { useState } from "react";
import {
  Row,
  Col,
  Typography,
  Form,
  Input,
  Select,
  Button,
  Switch,
  Divider,
} from "antd";
import { readLocalStorage } from "../../utils/readLocalStorage";
import ListOfRiders from "../../components/Tables/ListOfRiders";
import riderManagementStyle from "./style.module.css";
const officeLocation = JSON.parse(readLocalStorage("userData"));
const { locationObjects } = officeLocation || [];
const { Option } = Select;
const { Title } = Typography;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const RiderManagement = () => {
  const [form] = Form.useForm();
  const [isSwitch, setIsSwitch] = useState(true);

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
  };
  const onReset = () => {
    form.resetFields();
  };

  const onChangeSwitch = (checked) => {
    setIsSwitch(checked);
  };
  return (
    <>
      <Row>
        <Col xs={0} sm={0} md={10} lg={10} xl={10}></Col>
        <Col
          xs={24}
          sm={24}
          md={14}
          lg={14}
          xl={14}
          className={riderManagementStyle.topCol}
        >
          {/* <span>
            <Text type="secondary">Auto Refresh</Text> <ClockCircleFilled />
          </span>
          <span style={{ marginLeft: "10px" }}>
            <Text type="secondary">Last updated at {LoadTime}</Text>
          </span> */}
        </Col>
      </Row>
      <Row>
        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
          <Title level={4}>Rider Management</Title>
        </Col>
      </Row>
      <Row>
        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
          <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            initialValues={{
              residence: ["zhejiang", "hangzhou", "xihu"],
              prefix: "86",
            }}
            scrollToFirstError
          >
            <Form.Item
              name="firstName"
              label="First Name"
              rules={[
                {
                  required: true,
                  message: "first name is required!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              name="lastName"
              label="Last Name"
              rules={[
                {
                  required: true,
                  message: "last name is required!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="phoneNumber"
              label="Phone Number"
              rules={[
                {
                  required: true,
                  message: "phone number is required!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="empId"
              label="Emp ID"
              rules={[
                {
                  required: true,
                  message: "employee id is required!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="cnic"
              label="CNIC"
              rules={[
                {
                  required: true,
                  message: "CNIC id is required!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item name="status" label="Status">
              <Switch checked={isSwitch} onChange={onChangeSwitch} />
            </Form.Item>
            <Form.Item
              name="region"
              label="Region"
              rules={[{ required: true, message: "region  is required!" }]}
            >
              <Select placeholder="Region" allowClear>
                <Option value="clifton">Clifton</Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="department"
              label="Department"
              rules={[{ required: true, message: "department is required!" }]}
            >
              <Select placeholder="Department" allowClear>
                <Option value="sales">Sales</Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="office"
              label="Office"
              rules={[{ required: true, message: "office is required!" }]}
            >
              <Select placeholder="Department" allowClear>
                {locationObjects &&
                  locationObjects.map((office) => (
                    <Option
                      value={office.locationName}
                      key={office.locationName}
                    >
                      {office.locationName}
                    </Option>
                  ))}
              </Select>
            </Form.Item>

            <Form.Item {...tailFormItemLayout}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
              <Button
                type="primary"
                style={{ marginLeft: "10px" }}
                onClick={onReset}
              >
                Cancel
              </Button>
            </Form.Item>
          </Form>
        </Col>
      </Row>
      <Divider />
      <Row>
        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
          <Title level={4}>List of riders</Title>
        </Col>
      </Row>
      <Row>
        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
          <ListOfRiders />
        </Col>
      </Row>
    </>
  );
};

export default RiderManagement;
