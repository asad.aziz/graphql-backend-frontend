import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Row, Col, Typography, Menu } from "antd";
import overviewStyle from "./style.module.css";
import dayjs from "dayjs";
import { readLocalStorage } from "../../utils/readLocalStorage";
import HistoricData from "../../components/Subscreens/HistoricData";
import TotalCollectionCard from "../../components/Cards/TotalCollection";
import CurrentlyDueCard from "../../components/Cards/CurrentlyDue";
import CashInMachineCard from "../../components/Cards/CashInMachine";
import ActiveRidersCard from "../../components/Cards/ActiveRiders";
import LineChart from "../../components/Charts/VolumeVSNumber";
import DenominationBarChart from "../../components/Charts/DenominationBarChart";
import UnitwiseCashCollection from "../../components/Charts/UnitwiseCashCollection";
import CashCollectionStatus from "../../components/Charts/CashCollectionStatus";
import MachineStatus from "../../components/Charts/MachineStatus";
import CashDistributionChart from "../../components/Charts/CashDistributionChart";
import { clearAllStates } from "../../redux/getVolumeVsTimeTransactionsSlice";
const { Title } = Typography;

const OverView = () => {
  const today = dayjs().format("dddd, MMMM DD, YYYY").toString();
  const officeLocation = JSON.parse(readLocalStorage("userData"));
  const [officeName, setOfficeName] = useState("");
  const dispatch = useDispatch();
  const { locationObjects } = officeLocation || [];
  const changeOffice = (officeName) => {
    setOfficeName(officeName);
  };

  return (
    <>
      <Row>
        <Col xs={0} sm={0} md={10} lg={10} xl={10}></Col>
        <Col
          xs={24}
          sm={24}
          md={14}
          lg={14}
          xl={14}
          className={overviewStyle.topCol}
        >
          {/* <span>
            <Text type="secondary">Auto Refresh</Text> <ClockCircleFilled />
          </span>
          <span style={{ marginLeft: "10px" }}>
            <Text type="secondary">Last updated at {LoadTime}</Text>
          </span> */}
        </Col>
      </Row>
      <Row>
        <Col xs={24} sm={24} md={14} lg={14} xl={14}>
          <Title level={4}>Today</Title>
          <p className={overviewStyle.dateColor}>{today}</p>
        </Col>

        <Col xs={24} sm={24} md={10} lg={10} xl={10}>
          <Menu
            defaultSelectedKeys={["overall"]}
            mode="horizontal"
            style={{ background: "transparent" }}
          >
            <Menu.Item
              key="overall"
              onClick={() => {
                changeOffice("Overall");
              }}
            >
              Overall
            </Menu.Item>
            {locationObjects &&
              locationObjects.map((office) => (
                <Menu.Item
                  key={office.locationName}
                  onClick={() => {
                    dispatch(clearAllStates());
                    changeOffice(office.locationName);
                  }}
                >
                  {office.locationName}
                </Menu.Item>
              ))}
          </Menu>
        </Col>
      </Row>

      <Row className={overviewStyle.mainCotainerCards}>
        <Col xs={24} sm={12} md={6} lg={6} xl={6}>
          <TotalCollectionCard officeName={officeName} />
        </Col>
        <Col xs={24} sm={12} md={6} lg={6} xl={6}>
          <CurrentlyDueCard />
        </Col>
        <Col xs={24} sm={12} md={6} lg={6} xl={6}>
          <CashInMachineCard officeName={officeName} />
        </Col>
        <Col xs={24} sm={12} md={6} lg={6} xl={6}>
          <ActiveRidersCard officeName={officeName} />
        </Col>
      </Row>

      <Row className={overviewStyle.subCotainerCards1}>
        <Col xs={24} sm={24} md={24} lg={11} xl={11}>
          <p className={overviewStyle.subContainerCardHeading1}>
            Time of the day - Volume vs Number of transactions
          </p>
          <LineChart officeName={officeName} />
        </Col>
        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
          <p className={overviewStyle.subContainerCardHeading1}>
            Cash Denominations
          </p>
          <DenominationBarChart officeName={officeName} />
        </Col>
        <Col xs={24} sm={24} md={24} lg={5} xl={5}>
          <p className={overviewStyle.subContainerCardHeading1}>
            Unit wise Cash Collection
          </p>
          <UnitwiseCashCollection officeName={officeName} />
        </Col>
      </Row>

      <Row className={overviewStyle.subCotainerCards1}>
        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
          <p className={overviewStyle.subContainerCardHeading1}>
            Cash Collection Status (Coming Soon)
          </p>
          <CashCollectionStatus />
        </Col>
        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
          <p className={overviewStyle.subContainerCardHeading1}>
            Machine Status (Coming Soon)
          </p>
          <MachineStatus />
        </Col>
        <Col xs={24} sm={24} md={24} lg={8} xl={8}>
          <p className={overviewStyle.subContainerCardHeading1}>
            Cash Shortage distribution (Coming Soon)
          </p>
          <CashDistributionChart />
        </Col>
      </Row>
      <Row className={overviewStyle.subCotainerCards1}>
        <Col xs={24} sm={24} md={24} lg={24} xl={24}>
          <HistoricData officeName={officeName} />
        </Col>
      </Row>
    </>
  );
};

export default OverView;
