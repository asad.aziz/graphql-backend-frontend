import React, { useState, useEffect } from "react";
import { Route, Switch, withRouter, useHistory } from "react-router-dom";
import { Layout, Menu, Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { HomeOutlined, ShopOutlined, TeamOutlined } from "@ant-design/icons";
import Header from "../../components/Header";

import OverView from "../OverView";
import Error404 from "../../components/ErrorHandlers/404";
import UpdatePassword from "../UpdatePassword";
import Teller from "../Teller";
import RiderManagement from "../RiderManagement";

import { readLocalStorage } from "../../utils/readLocalStorage";
import AppStyle from "./style.module.css";
import { useIdleTimer } from "react-idle-timer";
import {
  refreshTokenApi,
  selectRefreshToken,
} from "../../redux/refreshTokenSlice";

import {
  getCompanyLogo,
  selectCompanyDetails,
} from "../../redux/getCompanyLogo";

const { Content, Sider } = Layout;

const Dashboard = () => {
  const history = useHistory();
  const isMenuVisible = JSON.parse(readLocalStorage("userData"));
  const refreshTokenStatus = useSelector(selectRefreshToken);
  const companyLogoDetails = useSelector(selectCompanyDetails);
  const [logoUrl, setLogoUrl] = useState(null);
  const dispatch = useDispatch();

  const [collapsed, setCollapsed] = useState(false);
  const [userInactive, setUserInactive] = useState(false);

  const menuKey = readLocalStorage("menuKey");
  useEffect(() => {
    dispatch(getCompanyLogo());
  }, []);
  useEffect(() => {
    showLogoutWarningModal();
  }, [userInactive]);

  useEffect(() => {
    if (refreshTokenStatus) {
      if (
        refreshTokenStatus.refreshTokenDetails.tokenStatus === "unchanged" ||
        refreshTokenStatus.refreshTokenDetails.tokenStatus === "error"
      ) {
        history.push("/login");
      }
    }
  }, [refreshTokenStatus]);

  useEffect(() => {
    if (companyLogoDetails && companyLogoDetails.companyDetails) {
      setLogoUrl(companyLogoDetails.companyDetails);
    }
  }, [companyLogoDetails]);
  const toggle = () => {
    setCollapsed((prevCollapsed) => !prevCollapsed);
  };

  const showLogoutWarningModal = () => {
    isIdle();
    if (userInactive === true) {
      let secondsToGo = 10;
      const modal = Modal.warning({
        title: "Session Expiring",
        okCancel: true,
        content:
          "Your session will be expired due to inactivity. Do you wish to remain logged in?",
        okText: "Yes",
        cancelText: "No",
        onOk() {
          handleYes();
        },
        onCancel() {
          handleNo();
        },
      });

      let timer = setInterval(() => {
        secondsToGo--;
        if (secondsToGo > 0 && localStorage.length !== 0) {
          modal.update({
            content: (
              <div>
                <p>
                  Your session will be expired due to inactivity. Do you wish to
                  remain logged in?
                </p>
                <p>You will be logged out after {secondsToGo} second(s)</p>
              </div>
            ),
          });
        }
      }, 1000);

      const timeout = setTimeout(() => {
        modal.destroy();
        clearInterval(timer);
        handleNo();
      }, secondsToGo * 1000);

      const handleNo = () => {
        timer = null;
        modal.destroy();
        localStorage.clear();
        history.push("/login");
      };

      const handleYes = () => {
        dispatch(refreshTokenApi({ jwt: readLocalStorage("jwt") }));
        modal.destroy();
        secondsToGo = 0;
        clearTimeout(timeout);
      };
    }
  };

  const handleOnIdle = (event) => {
    getLastActiveTime();
    setUserInactive(true);
  };

  const handleOnActive = (event) => {
    getRemainingTime();
    setUserInactive(false);
  };

  const { getRemainingTime, getLastActiveTime, isIdle } = useIdleTimer({
    timeout: 1000 * 60 * 60 * 2,
    onIdle: handleOnIdle,
    onActive: handleOnActive,
  });

  return (
    <>
      <Layout>
        <Header toggle={toggle} collapsed={collapsed} />
        <Layout className="site-layout">
          {isMenuVisible && isMenuVisible.isPasswordChangeRequired ? null : (
            <Sider
              trigger={null}
              collapsible
              collapsed={collapsed}
              theme={"light"}
            >
              <div className={AppStyle.logo}>
                <img
                  src={
                    logoUrl !== null
                      ? logoUrl
                      : `${process.env.REACT_APP_EC2_INSTANCE_URL}xfoilLogo.png`
                  }
                  alt=""
                  className={
                    collapsed
                      ? `${AppStyle.collapsedLogoClass}`
                      : `${AppStyle.unCollapsedLogoClass}`
                  }
                />
              </div>
              <Menu
                theme="light"
                mode="inline"
                defaultSelectedKeys={menuKey === null ? ["1"] : [menuKey]}
              >
                <Menu.Item
                  key="1"
                  icon={<HomeOutlined />}
                  onClick={() => {
                    localStorage.setItem("menuKey", "1");
                    history.push("/app/overview");
                  }}
                >
                  Overview
                </Menu.Item>
                <Menu.Item
                  key="2"
                  icon={<ShopOutlined />}
                  onClick={() => {
                    localStorage.setItem("menuKey", "2");
                    history.push("/app/teller");
                  }}
                >
                  Teller
                </Menu.Item>
                <Menu.Item
                  key="3"
                  icon={<TeamOutlined />}
                  onClick={() => {
                    localStorage.setItem("menuKey", "3");
                    history.push("/app/rider_management");
                  }}
                >
                  {" "}
                  Rider Management
                </Menu.Item>
              </Menu>
            </Sider>
          )}
          <Content
            className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: "5px 24px 24px 10px",
              minHeight: "100vh",
              background: "#F2F9FD",
            }}
          >
            <Switch>
              <Route exact path="/app/overview" component={OverView} />
              <Route
                exact
                path="/app/update-password"
                component={UpdatePassword}
              />
              <Route exact path="/app/teller" component={Teller} />
              <Route
                exact
                path="/app/rider_management"
                component={RiderManagement}
              />
              <Route component={Error404} />
            </Switch>
          </Content>
        </Layout>
      </Layout>
      ;
    </>
  );
};

export default withRouter(Dashboard);
