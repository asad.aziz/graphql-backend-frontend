import React, { useEffect, useState } from "react";
import { Form, Input, Button, Checkbox, Typography, Spin } from "antd";
import { withRouter } from "react-router";
import { useHistory, Redirect } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { login, selectLogin } from "../../redux/loginSlice";
import { clearAllStates } from "../../redux/logoutSlice";
import { readLocalStorage } from "../../utils/readLocalStorage";

import loginScreenStyle from "./style.module.css";

const layout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
};
const tailLayout = {
  wrapperCol: {
    offset: 6,
    span: 12,
  },
};

const { Title } = Typography;

const Login = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const loginStatus = useSelector(selectLogin);
  const isJwt = readLocalStorage("jwt");
  const userData = JSON.parse(readLocalStorage("userData"));
  const [incorrectCredentials, setIncorrectCredentials] = useState(false);
  const [serverError, setServerError] = useState(false);
  const [isLogginContinue, setIsLogginContinue] = useState(false);
  const onFinish = (values) => {
    console.log(process.env);
    dispatch(clearAllStates());
    setIncorrectCredentials(false);
    setServerError(false);
    dispatch(login(values));
    setIsLogginContinue(true);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {
    if (loginStatus && loginStatus.loginDetails.status === 202) {
      if (userData && userData.isPasswordChangeRequired) {
        history.push("/app/update-password");
        setIsLogginContinue(false);
      } else if (userData && userData.isPasswordChangeRequired !== true) {
        history.push("/app/overview");
        setIsLogginContinue(false);
      }
    } else if (loginStatus && loginStatus.loginDetails.status === 401) {
      setIncorrectCredentials(true);
      setIsLogginContinue(false);
    } else if (loginStatus && loginStatus.loginDetails.status === 500) {
      setServerError(true);
      setIsLogginContinue(false);
    }
  }, [loginStatus]);
  if (isJwt) {
    return (
      <Redirect
        to={{
          pathname: "/app/overview",
        }}
      />
    );
  } else {
    return (
      <div className={loginScreenStyle.mainContainer}>
        <Title level={2} className={loginScreenStyle.loginHeading}>
          Dashboard Login
        </Title>
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: "Email is required!",
              },
              {
                type: "email",
                message: "Please input your correct email!",
              },
            ]}
          >
            <Input type="email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout} name="remember" valuePropName="checked">
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <Form.Item {...tailLayout}>
            {incorrectCredentials ? (
              <p className={loginScreenStyle.errorStyling}>
                Email or password is incorrect
              </p>
            ) : null}
            {serverError ? (
              <p className={loginScreenStyle.errorStyling}>
                Something went wrong .Please try again
              </p>
            ) : null}
            {isLogginContinue ? (
              <Spin size="small" />
            ) : (
              <Button
                type="primary"
                htmlType="submit"
                className={loginScreenStyle.loginFormButton}
              >
                Login
              </Button>
            )}
          </Form.Item>
        </Form>
      </div>
    );
  }
};

export default withRouter(Login);
