/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable no-trailing-spaces */
/* eslint-disable operator-linebreak */
import React, { useState, useEffect } from "react";
import { Button } from "antd";
import ExcelJS from "exceljs";
import saveAs from "file-saver";
import moment from "moment";

function ExcelGeneratorButton(props) {
  const [transformedAccepted, setTransformedAccepted] = useState([]);
  const [transformedRejected, setTransformedRejected] = useState([]);
  const [transformedTotal, setTransformedTotal] = useState([]);
  const [riders, setRiders] = useState([]);

  const toLocalTime = (transDate, lastUpdated) => {
    const utcTime = moment
      .utc(
        `${transDate.year}-${transDate.month}-${transDate.day}` +
          " " +
          `${lastUpdated.hour}:${lastUpdated.minute}:${lastUpdated.second}`
      )
      .format("YYYY-MM-DD HH:mm:ss");
    let localTime = moment.utc(utcTime).toDate();
    localTime = moment(localTime).format("HH:mm:ss");
    return localTime;
  };

  useEffect(() => {
    const sortedDenominations = props.excelData;
    console.log("SortedDEnominations ", sortedDenominations);
    const transformed = [];
    const rTransformed = [];
    const tTransformed = [];
    const ridersList = [];

    if (sortedDenominations && sortedDenominations.length > 0) {
      for (let i = 0; i < sortedDenominations.length; i++) {
        transformed.push(sortedDenominations[i].accepted);
        rTransformed.push(sortedDenominations[i].rejected);
        tTransformed.push(sortedDenominations[i].totalCounts);

        if (sortedDenominations[i].riders && sortedDenominations[i].riders) {
          const riderAggregated = sortedDenominations[i].riders;
          ridersList.push({
            id: riderAggregated.id,
            firstName: riderAggregated.firstName,
            lastName: riderAggregated.lastName,
            //   lastUpdatedAt: toLocalTime(
            //     riderAggregated.transDate,
            //     riderAggregated.lastUpdated
            //   ),
          });
          //   }
        }
      }

      setRiders(ridersList);
      setTransformedAccepted(transformed);
      setTransformedRejected(rTransformed);
      setTransformedTotal(tTransformed);
    }
  }, [props]);

  const excelExport = (AcceptedNotes, RejectedNotes, TotalNotes, riders) => {
    const ExcelJSWorkbook = new ExcelJS.Workbook();
    // const worksheet = ExcelJSWorkbook.addWorksheet("Denominations", {});
    const worksheetRiderAggregated = ExcelJSWorkbook.addWorksheet(
      "Rider Aggregated Table"
    );

    worksheetRiderAggregated.mergeCells("A1:G1");
    const customCellSubHeadingRider = worksheetRiderAggregated.getCell("A1");
    customCellSubHeadingRider.font = {
      name: "Cambria",
      family: 4,
      size: 14,
      bold: true,
    };
    customCellSubHeadingRider.alignment = {
      vertical: "middle",
      horizontal: "center",
    };
    customCellSubHeadingRider.value = "Rider Aggregated Table";

    const headerRowRider = worksheetRiderAggregated.addRow();
    const riderId = headerRowRider.getCell(1);
    const firstName = headerRowRider.getCell(2);
    const lastName = headerRowRider.getCell(3);
    const cashInMachine = headerRowRider.getCell(4);
    const cashInHand = headerRowRider.getCell(5);
    const total = headerRowRider.getCell(6);
    const lastUpdatedAt = headerRowRider.getCell(7);

    riderId.value = "Rider Id";
    firstName.value = "First Name";
    lastName.value = "Last Name";
    cashInMachine.value = "Cash In Machine";
    cashInHand.value = "Cash In Hand";
    total.value = "Total";
    lastUpdatedAt.value = "Last Updated At";

    for (let iter = 0; iter < riders.length; iter++) {
      const currentRider = riders[iter];
      const DataRowRider = worksheetRiderAggregated.addRow();
      const riderId = DataRowRider.getCell(1);
      const firstName = DataRowRider.getCell(2);
      const lastName = DataRowRider.getCell(3);
      const cashInMachine = DataRowRider.getCell(4);
      const cashInHand = DataRowRider.getCell(5);
      const total = DataRowRider.getCell(6);
      const lastUpdatedAt = DataRowRider.getCell(7);

      riderId.value = currentRider.id;
      firstName.value = currentRider.firstName;
      lastName.value = currentRider.lastName;
      cashInMachine.value = AcceptedNotes[iter].sum;
      cashInHand.value = RejectedNotes[iter].sum;
      total.value = AcceptedNotes[iter].sum + RejectedNotes[iter].sum;
      AcceptedNotes[iter].transDate !== undefined
        ? (lastUpdatedAt.value = toLocalTime(
            AcceptedNotes[iter].transDate,
            AcceptedNotes[iter].lastUpdated
          ))
        : (lastUpdatedAt.value = toLocalTime(
            RejectedNotes[iter].transDate,
            RejectedNotes[iter].lastUpdated
          ));
    }

    console.log("workbook excel object ", worksheetRiderAggregated.columns);
    worksheetRiderAggregated.columns.forEach(function (column, i) {
      var maxLength = 0;
      debugger;
      column.eachCell({ includeEmpty: true }, function (cell) {
        debugger;
        var columnLength = cell.value ? cell.value.toString().length : 10;
        if (columnLength > maxLength) {
          maxLength = columnLength;
        }
      });
      column.width = maxLength < 10 ? 10 : maxLength;
    });

    ExcelJSWorkbook.xlsx.writeBuffer().then(function (buffer) {
      saveAs(
        new Blob([buffer], { type: "application/octet-stream" }),
        `Rider Aggregated Report.xlsx`
      );
    });
  };

  return (
    <>
      {transformedAccepted.length &&
      transformedRejected.length &&
      transformedTotal.length &&
      riders.length > 0 ? (
        <Button
          style={{ marginTop: "1em", float: "right", marginRight: "2em" }}
          type="primary"
          onClick={() => {
            excelExport(
              transformedAccepted,
              transformedRejected,
              transformedTotal,
              riders
            );
          }}
        >
          Download Excel
        </Button>
      ) : null}
    </>
  );
}

export default ExcelGeneratorButton;
