import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import dayjs from "dayjs";

import DatePicker from "../Custom/DatePicker";
import { dateDetails } from "../../redux/dateSlice";

function DatePickerCustom() {
  const dateString = new Date();
  const dispatch = useDispatch();

  const onDateChange = (date) => {
    if (date !== null || date !== undefined) {
      date = date.format("DD-MM-YYYY").toString();
      localStorage.setItem("selectedDate", date);
      dispatch(dateDetails(date));
    }
  };

  const disabledDate = (current) => {
    const dayDate = dayjs(dateString);
    if (current.isAfter(dayDate)) return current;
  };

  useEffect(() => {
    localStorage.setItem(
      "selectedDate",
      dayjs(dateString).format("DD-MM-YYYY").toString()
    );
    const todayDate = dayjs(dateString).format("DD-MM-YYYY").toString();
    dispatch(dateDetails(todayDate));
  }, []);

  return (
    <>
      <DatePicker
        size={"small"}
        onChange={onDateChange}
        defaultValue={dayjs(dateString)}
        format={"DD-MM-YYYY"}
        disabledDate={disabledDate}
      />
    </>
  );
}

export default DatePickerCustom;
