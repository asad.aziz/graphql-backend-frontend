import React from "react";

// import DatePickerCustom from "../../components/Custom/DatePickerCustom";
import TransactionTable from "../Charts/TransactionTable";
import PropTypes from "prop-types";
function HistoricData(props) {
  const { officeName } = props;

  return (
    <>
      {/* <DatePickerCustom /> */}

      <TransactionTable officeName={officeName} />
    </>
  );
}

export default HistoricData;
HistoricData.propTypes = {
  officeName: PropTypes.any,
};
