import React from "react";
import { Spin } from "antd";

import { LoadingOutlined } from "@ant-design/icons";

const Loader = (props) => {
  const antIcon = (
    <LoadingOutlined style={{ fontSize: 80, color: "#048998" }} />
  );
  return (
    <>
      <Spin
        size="large"
        indicator={antIcon}
        style={{ position: "absolute", left: "50%", top: "50%" }}
      />
    </>
  );
};
export default Loader;
