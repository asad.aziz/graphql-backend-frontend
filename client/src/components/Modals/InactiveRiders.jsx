import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Modal, Table, Input } from "antd";
import dayjs from "dayjs";
import PropTypes from "prop-types";
import {
  getInactiveRidersList,
  selectInactiveRiderDetails,
} from "../../redux/getInactiveRidersSlice";
import { getInactiveActiveRidersList } from "../../redux/getInactiveAndActiveRidersSlice";
import { readLocalStorage } from "../../utils/readLocalStorage";

import ButtonSpinner from "../../components/Loader/ButtonSpinner";
export default function InactiveRiders(props) {
  const { isModalVisible, handleOk, handleCancel, officeName } = props;
  const userData = JSON.parse(readLocalStorage("userData"));
  const { reportingKey } = userData;
  const dispatch = useDispatch();
  const getInactiveRidersListData = useSelector(selectInactiveRiderDetails);
  const [dataSource, setDataSource] = useState([]);
  const [isloader, setIsLoader] = useState(false);
  const columns = [
    {
      title: "Rider Id",
      dataIndex: `${reportingKey}`,
      key: `${reportingKey}`,
      render: (text, row, index) => {
        if (reportingKey === "cnic") {
          return text.replace(/(\d{5})(\d{7})/g, "$1-$2-");
        } else if (reportingKey === "phone") {
          return text.replace(/(\d{4})(\d{3})/g, "$1-$2-");
        } else {
          return text;
        }
      },
    },
    {
      title: "First Name",
      dataIndex: "firstName",
      key: "firstName",
    },
    {
      title: "Last Name",
      dataIndex: "lastName",
      key: "lastName",
    },
  ];

  const onSearch = (e) => {
    const currValue = e.target.value;
    if (currValue) {
      const filteredData = dataSource.filter((entry) =>
        entry[reportingKey].includes(currValue)
      );
      setDataSource(filteredData);
    } else {
      setDataSource(
        getInactiveRidersListData && getInactiveRidersListData.inActiveriderList
      );
    }
  };

  useEffect(() => {
    if (isModalVisible) {
      const todayDateStr = dayjs().format("DD-MM-YYYY").toString();
      if (officeName) {
        dispatch(
          getInactiveRidersList({
            date: todayDateStr,
            officeName: officeName,
          })
        );
        dispatch(
          getInactiveActiveRidersList({
            date: todayDateStr,
            officeName: officeName,
          })
        );
      } else {
        dispatch(
          getInactiveRidersList({
            date: todayDateStr,
          })
        );
        dispatch(
          getInactiveActiveRidersList({
            date: todayDateStr,
          })
        );
      }
    }
  }, [isModalVisible, officeName]);

  useEffect(() => {
    if (getInactiveRidersListData.isloader) {
      setIsLoader(true);
    } else {
      setDataSource(getInactiveRidersListData.inActiveriderList);
      setIsLoader(false);
    }
  }, [getInactiveRidersListData]);

  return (
    <div>
      <>
        <Modal
          title={
            dataSource.length
              ? `Inactive Riders - ${dataSource.length}`
              : `Inactive Riders - 0`
          }
          visible={isModalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
        >
          {isloader ? (
            <ButtonSpinner />
          ) : (
            <>
              <Input
                placeholder={`Search Rider by ${reportingKey}`}
                allowClear
                //  onSearch={onSearch}
                onChange={onSearch}
                style={{ marginBottom: "10px" }}
              />
              <Table
                columns={columns}
                dataSource={dataSource}
                size={"small"}
                pagination={{ position: ["bottomCenter"], pageSize: "5" }}
              />
            </>
          )}
        </Modal>
      </>
    </div>
  );
}

InactiveRiders.propTypes = {
  isModalVisible: PropTypes.bool,
  handleOk: PropTypes.func,
  handleCancel: PropTypes.func,
  inActiverRiderDetail: PropTypes.any,
  officeName: PropTypes.string,
};

// import { useDispatch } from "react-redux";

// const InActiveRidersModal = (props) => {
//   const { isModal, isModalOpen, inActiverRiderDetail, clearState } = props;
//   const [isModalVisible, setIsModalVisible] = useState(false);

//   const dispatch = useDispatch();
//   useEffect(() => {
//     setIsModalVisible(isModal);
//   }, [isModal]);

//   useEffect(() => {
//     //setDataSource(data);
//     setDataSource(inActiverRiderDetail.inActiveRiders);
//   }, [inActiverRiderDetail]);

//   const handleOk = () => {
//     setIsModalVisible(false);
//     dispatch(clearState());
//   };

//   const handleCancel = () => {
//     setIsModalVisible(false);
//     dispatch(clearState());
//   };

//   return (
//     <>

//     </>
//   );
// };

// export default InActiveRidersModal;
