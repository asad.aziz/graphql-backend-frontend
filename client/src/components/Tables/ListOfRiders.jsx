import React from "react";

import { Table, Input } from "antd";

import { useQuery, gql } from "@apollo/client";

import Loader from "../../components/Loader/ButtonSpinner";
import { readLocalStorage } from "../../utils/readLocalStorage";
const { Search } = Input;

const columns = [
  {
    title: "Full Name",
    dataIndex: "first_name",
    key: "first_name",
  },
  {
    title: "Phone Number",
    dataIndex: "phone_number",
    key: "phone_number",
  },
  {
    title: "EMP ID",
    dataIndex: "delivery_agent_id",
    key: "delivery_agent_id",
  },
  {
    title: "CNIC",
    dataIndex: "cnic",
    key: "cnic",
  },
  {
    title: "Status",
    dataIndex: "is_rider_profile_active",
    key: "is_rider_profile_active",
    // eslint-disable-next-line react/display-name
    render: (text, row, index) => {
      return <>{row.is_rider_profile_active ? "Active" : "Inactive"}</>;
    },
  },
  {
    title: "Region",
    dataIndex: "region",
    key: "region",
  },
  {
    title: "Department",
    dataIndex: "department",
    key: "department",
  },
  {
    title: "Office",
    dataIndex: "office_location",
    key: "office_location",
  },
];

const ExchangeRates = () => {
  const user = JSON.parse(readLocalStorage("userData"));
  const { companyId } = user || [];
  console.log(typeof companyId);
  const { loading, error, data } = useQuery(gql`
    {
      rider(id: ${companyId}) {
        id
        cnic
        delivery_agent_id
        first_name
        is_rider_profile_active
        last_name
        phone_number
        company_id
        email
        office_location
      }
    }
  `);

  if (loading) {
    return (
      <p>
        <Loader />
      </p>
    );
  }
  if (error) return <p>Error :(</p>;
  if (data) console.log(data);
  return (
    <>
      <Table dataSource={data && data.rider} columns={columns} />
    </>
  );
  // data.rider.map(({ id }) => (
  //   <div key={id}>
  //     <p>{id}</p>
  //   </div>
  // ));
};
const ListOfRiders = () => {
  return (
    <>
      <Search
        placeholder="Rider details to search"
        allowClear
        style={{ width: 300 }}
      />
      <ExchangeRates />
    </>
  );
};

export default ListOfRiders;
