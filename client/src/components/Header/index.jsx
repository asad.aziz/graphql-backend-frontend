import React from "react";
import PropTypes from "prop-types";
import { Layout, Row, Col, Space, Dropdown, Menu } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  QuestionCircleOutlined,
  BellOutlined,
} from "@ant-design/icons";
import { readLocalStorage } from "../../utils/readLocalStorage";

import { logout, clearAllStates } from "../../redux/logoutSlice";
import { useHistory } from "react-router-dom";

import { useDispatch } from "react-redux";

import HeaderStyling from "./style.module.css";
const { Header } = Layout;
const HeaderComppnent = (props) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const userData = JSON.parse(readLocalStorage("userData"));
  const { toggle, collapsed } = props;

  const logoutUser = () => {
    dispatch(logout());
    dispatch(clearAllStates());
    localStorage.clear();
    history.push("/login");
  };

  const menu = (
    <Menu>
      <Menu.Item>
        <div onClick={logoutUser}>Logout</div>
      </Menu.Item>
      <Menu.Item>
        <div
          onClick={() => {
            history.push("/app/update-password");
          }}
        >
          Update Password
        </div>
      </Menu.Item>
    </Menu>
  );

  return (
    <Header
      className={HeaderStyling.siteLayoutBackground}
      style={{ padding: 0 }}
    >
      <Row>
        <Col xs={4} sm={4} md={6} lg={6} xl={6}>
          <span className={HeaderStyling.customTrigger}>
            {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: toggle,
              }
            )}
          </span>
        </Col>
        <Col
          xs={4}
          sm={7}
          md={7}
          lg={7}
          xl={7}
          style={{ textAlign: "center" }}
        ></Col>
        <Col xs={16} sm={11} md={11} lg={11} xl={11}>
          <div
            className="header-links"
            style={{ float: "right", marginRight: "20px" }}
          >
            <Space size={15}>
              <QuestionCircleOutlined />
              <BellOutlined />
              <span>
                <img src="/avatar.png" alt="" />
              </span>
            </Space>
            <span
              style={{
                marginLeft: "10px",
                fontSize: "14px",
                fontWeight: "400",
              }}
            >
              <Dropdown overlay={menu}>
                <a
                  className="ant-dropdown-link"
                  onClick={(e) => e.preventDefault()}
                  style={{ color: "black", fontWeight: "600" }}
                >
                  {userData && userData.firstNameAdmin}{" "}
                  {userData && userData.lastNameAdmin}
                </a>
              </Dropdown>
            </span>
          </div>
        </Col>
      </Row>
    </Header>
  );
};

HeaderComppnent.propTypes = {
  collapsed: PropTypes.bool,
  toggle: PropTypes.func,
};
export default HeaderComppnent;
