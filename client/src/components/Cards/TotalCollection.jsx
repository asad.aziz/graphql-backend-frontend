import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import PropTypes from "prop-types";
import { numberFormatter } from "../../utils/numberFormatter";

import { Card } from "antd";
import { selectTotalCollection } from "../../redux/getTotalCollection";
import ButtonSpinner from "../../components/Loader/ButtonSpinner";
import stylings from "./style.module.css";

export default function TotalCollection(props) {
  const getTotalCollectionStatus = useSelector(selectTotalCollection);
  const [isloader, setIsLoader] = useState(false);
  const [totalCollection, setTotalCollection] = useState(0);
  useEffect(() => {
    if (getTotalCollectionStatus.isloader) {
      setIsLoader(true);
    } else if (
      getTotalCollectionStatus.isloader === false &&
      getTotalCollectionStatus.totalCollection
    ) {
      const total =
        getTotalCollectionStatus.totalCollection.cashInMachine +
        getTotalCollectionStatus.totalCollection.cashInHand;
      setTotalCollection(total);

      setIsLoader(false);
    }
  }, [getTotalCollectionStatus]);

  return (
    <div>
      <Card size="small" className={stylings.heroCard}>
        <p className={stylings.cardHeading}>Total Collection</p>
        {isloader ? (
          <ButtonSpinner />
        ) : (
          <p className={stylings.cardNumber}>
            {numberFormatter(totalCollection)}
          </p>
        )}
      </Card>
    </div>
  );
}
TotalCollection.propTypes = {
  offficeName: PropTypes.any,
};
