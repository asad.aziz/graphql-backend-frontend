/* eslint-disable indent */

import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Card } from "antd";
import { numberFormatter } from "../../utils/numberFormatter";
import InactiveRiders from "../Modals/InactiveRiders";
import ButtonSpinner from "../../components/Loader/ButtonSpinner";
import dayjs from "dayjs";
import PropTypes from "prop-types";
import {
  selectInactiveRiderDetails,
  getInactiveActiveRidersList,
} from "../../redux/getInactiveAndActiveRidersSlice";
import stylings from "./style.module.css";
export default function ActiveRiders(props) {
  const { officeName } = props;
  const getInactiveActiveRidersListData = useSelector(
    selectInactiveRiderDetails
  );
  const dispatch = useDispatch();
  const [isloader, setIsLoader] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const todayDateStr = dayjs().format("DD-MM-YYYY").toString();
  useEffect(() => {
    if (getInactiveActiveRidersListData.isloader === null) {
      dispatch(getInactiveActiveRidersList({ date: todayDateStr }));
    }
  }, []);
  useEffect(() => {
    if (officeName !== "") {
      dispatch(
        getInactiveActiveRidersList({
          date: todayDateStr,
          officeName: officeName,
        })
      );
    }
  }, [officeName]);
  useEffect(() => {
    if (getInactiveActiveRidersListData.isloader) {
      setIsLoader(true);
    } else {
      setIsLoader(false);
    }
  }, [getInactiveActiveRidersListData]);
  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <div>
      <Card size="small" className={stylings.counterCard3}>
        <p className={stylings.cardHeading}>
          Active Riders{" "}
          <span className={stylings.cardSubHeading}>(Pending Riders)</span>
        </p>

        {isloader ? (
          <ButtonSpinner />
        ) : isloader &&
          getInactiveActiveRidersListData.activeInActiveriderList.length < 0 ? (
          <ButtonSpinner />
        ) : (
          <>
            {getInactiveActiveRidersListData &&
            getInactiveActiveRidersListData.activeInActiveriderList ? (
              <p className={stylings.cardNumber}>
                {
                  getInactiveActiveRidersListData.activeInActiveriderList
                    .activeRidersCount
                }

                {getInactiveActiveRidersListData.activeInActiveriderList ? (
                  <span
                    className={stylings.cardSubHeading}
                    style={{ cursor: "pointer" }}
                    onClick={showModal}
                  >
                    ({" "}
                    {numberFormatter(
                      getInactiveActiveRidersListData.activeInActiveriderList
                        .inActiveRidersCount
                    )}{" "}
                    )
                  </span>
                ) : (
                  <span className={stylings.cardSubHeading}>(0)</span>
                )}
              </p>
            ) : (
              <ButtonSpinner />
            )}
          </>
        )}
        <InactiveRiders
          isModalVisible={isModalVisible}
          showModal={showModal}
          handleOk={handleOk}
          handleCancel={handleCancel}
          officeName={officeName}
        />
      </Card>
    </div>
  );
}
ActiveRiders.propTypes = {
  officeName: PropTypes.any,
};
