import React from "react";
import { Card } from "antd";
// import { numberFormatter } from "../../utils/numberFormatter";
import stylings from "./style.module.css";
export default function CurrentlyDue() {
  return (
    <div>
      <Card size="small" className={stylings.conuterCard1}>
        <p className={stylings.cardHeading}>
          Currently Due{" "}
          <span className={stylings.cardSubHeading}>(Total Due today)</span>{" "}
        </p>
        <p className={stylings.cardNumber}>
          No Data
          {/* {numberFormatter(3780000)} */}
          <span className={stylings.cardSubHeading}>
            (No Data)
            {/* ({numberFormatter(5124000)}) */}
          </span>
        </p>
      </Card>
    </div>
  );
}
