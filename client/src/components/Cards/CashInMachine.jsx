import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Card } from "antd";
import PropTypes from "prop-types";
import { numberFormatter } from "../../utils/numberFormatter";
import dayjs from "dayjs";
import {
  getTotalCollection,
  selectTotalCollection,
} from "../../redux/getTotalCollection";
import ButtonSpinner from "../../components/Loader/ButtonSpinner";
import stylings from "./style.module.css";

export default function CashInMachine(props) {
  const { officeName } = props;
  const getTotalCollectionStatus = useSelector(selectTotalCollection);
  const dispatch = useDispatch();

  const [isloader, setIsLoader] = useState(false);
  const [totalCollection, setTotalCollection] = useState(0);
  const [totalCollectionInHand, setTotalCollectionInHand] = useState(0);
  const date = dayjs().format("DD-MM-YYYY").toString();

  useEffect(() => {
    if (officeName !== "") {
      dispatch(getTotalCollection({ date: date, officeName: officeName }));
    }
  }, [officeName]);

  useEffect(() => {
    if (getTotalCollectionStatus.isloader === null) {
      dispatch(getTotalCollection({ date: date, officeName: officeName }));
    }
  }, []);

  useEffect(() => {
    if (getTotalCollectionStatus.isloader) {
      setIsLoader(true);
    } else {
      setTotalCollection(
        getTotalCollectionStatus.totalCollection &&
          getTotalCollectionStatus.totalCollection.cashInMachine
      );
      setTotalCollectionInHand(
        getTotalCollectionStatus.totalCollection &&
          getTotalCollectionStatus.totalCollection.cashInHand
      );
      setIsLoader(false);
    }
  }, [getTotalCollectionStatus]);
  return (
    <div>
      <Card size="small" className={stylings.counterCard2}>
        <p className={stylings.cardHeading}>
          Cash in Machine{" "}
          <span className={stylings.cardSubHeading}>(in Hand)</span>
        </p>
        {isloader ? (
          <ButtonSpinner />
        ) : (
          <p className={stylings.cardNumber}>
            {totalCollection
              ? numberFormatter(`${totalCollection}`)
              : "No Data"}
            <span className={stylings.cardSubHeading}>
              (
              {totalCollectionInHand
                ? numberFormatter(`${totalCollectionInHand}`)
                : "No Data"}
              ){/* ( {numberFormatter(121000)}) */}
            </span>
          </p>
        )}
      </Card>
    </div>
  );
}
CashInMachine.propTypes = {
  officeName: PropTypes.any,
};
