import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import dayjs from "dayjs";
import { Col } from "antd";
import tellerStyle from "../../screens/Teller/style.module.css";
import { numberFormatter } from "../../utils/numberFormatter";
import { getDeno, selectDeno } from "../../redux/getDenominationsCount";
export default function CashDenoInHand() {
  const dispatch = useDispatch();
  const getCashDenoCount = useSelector(selectDeno);
  const todayDateStr = dayjs().format("DD-MM-YYYY").toString();
  const [denoInHand, setDenoInHand] = useState({
    10: 0,
    20: 0,
    50: 0,
    100: 0,
    500: 0,
    1000: 0,
    5000: 0,
  });
  useEffect(() => {
    if (getCashDenoCount.isloader === null) {
      dispatch(getDeno({ date: todayDateStr }));
    }
  }, []);
  useEffect(() => {
    if (getCashDenoCount && getCashDenoCount.denominationsList.length) {
      const notesCounter = {
        10: 0,
        20: 0,
        50: 0,
        100: 0,
        500: 0,
        1000: 0,
        5000: 0,
      };
      const denoList = getCashDenoCount.denominationsList;
      for (let i = 0; i < denoList.length; i++) {
        if (denoList[i].isManual) {
          notesCounter[10] += denoList[i]._10;
          notesCounter[20] += denoList[i]._20;
          notesCounter[50] += denoList[i]._50;
          notesCounter[100] += denoList[i]._100;
          notesCounter[500] += denoList[i]._500;
          notesCounter[1000] += denoList[i]._1000;
          notesCounter[5000] += denoList[i]._5000;
        }
      }
      setDenoInHand(notesCounter);
    }
  }, [getCashDenoCount]);
  return (
    <>
      <Col
        xs={3}
        sm={3}
        md={3}
        lg={3}
        xl={3}
        className={tellerStyle.cashDenoBox}
      >
        <p className={tellerStyle.cashDenoHeading}>10</p>
        <p className={tellerStyle.cashDenoValue}>
          {numberFormatter(denoInHand[10])}
        </p>
      </Col>
      <Col
        xs={3}
        sm={3}
        md={3}
        lg={3}
        xl={3}
        className={tellerStyle.cashDenoBox}
      >
        <p className={tellerStyle.cashDenoHeading}>20</p>
        <p className={tellerStyle.cashDenoValue}>
          {numberFormatter(denoInHand[20])}
        </p>
      </Col>
      <Col
        xs={3}
        sm={3}
        md={3}
        lg={3}
        xl={3}
        className={tellerStyle.cashDenoBox}
      >
        <p className={tellerStyle.cashDenoHeading}>50</p>
        <p className={tellerStyle.cashDenoValue}>
          {numberFormatter(denoInHand[50])}
        </p>
      </Col>
      <Col
        xs={3}
        sm={3}
        md={3}
        lg={3}
        xl={3}
        className={tellerStyle.cashDenoBox}
      >
        <p className={tellerStyle.cashDenoHeading}>100</p>
        <p className={tellerStyle.cashDenoValue}>
          {numberFormatter(denoInHand[100])}
        </p>
      </Col>
      <Col
        xs={3}
        sm={3}
        md={3}
        lg={3}
        xl={3}
        className={tellerStyle.cashDenoBox}
      >
        <p className={tellerStyle.cashDenoHeading}>500</p>
        <p className={tellerStyle.cashDenoValue}>
          {numberFormatter(denoInHand[500])}
        </p>
      </Col>
      <Col
        xs={3}
        sm={3}
        md={3}
        lg={3}
        xl={3}
        className={tellerStyle.cashDenoBox}
      >
        <p className={tellerStyle.cashDenoHeading}>1000</p>
        <p className={tellerStyle.cashDenoValue}>
          {numberFormatter(denoInHand[1000])}
        </p>
      </Col>
      <Col
        xs={3}
        sm={3}
        md={3}
        lg={3}
        xl={3}
        className={tellerStyle.cashDenoBox}
      >
        <p className={tellerStyle.cashDenoHeading}>5000</p>
        <p className={tellerStyle.cashDenoValue}>
          {numberFormatter(denoInHand[5000])}
        </p>
      </Col>
    </>
  );
}
