/* eslint-disable indent */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  InputNumber,
  Select,
  Space,
  Row,
  Col,
  Checkbox,
} from "antd";
import { readLocalStorage } from "../../utils/readLocalStorage";
import _ from "lodash";
import { numberFormatter } from "../../utils/numberFormatter";
import {
  postReconcilationDetails,
  selectReconcilationDetails,
} from "../../redux/manualReconSlice";
import {
  searchedRidersList,
  selectSearchedRiders,
} from "../../redux/searchRiders";
import { getTotalCollection } from "../../redux/getTotalCollection";
import {
  getNumberOfNotes,
  selectNumberOfNotes,
} from "../../redux/storeNotesSlice";

import {
  selectTransactionDetails,
  transactionDetailsThunk,
} from "../../redux/transactionDetailsSlice";
import { getInactiveActiveRidersList } from "../../redux/getInactiveAndActiveRidersSlice";
import { getDeno } from "../../redux/getDenominationsCount";
import { clearAllStates } from "../../redux/riderSlice";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import stylings from "./style.module.css";

export default function AddManualRecon(props) {
  const reconcilationDetailsUpdateStatus = useSelector(
    selectReconcilationDetails
  );
  const transactionDetailsSelector = useSelector(selectTransactionDetails);
  const searchRidersStatus = useSelector(selectSearchedRiders);
  const originalNumberOfNotes = useSelector(selectNumberOfNotes);
  const [notesCount, setNotesCount] = useState({});
  const [modalData, setModalData] = useState();
  const [totalSum, setTotalSum] = useState(0);
  const [acceptedSum, setAcceptedSum] = useState(0);
  const [buttonState, setButtonState] = useState(true);
  const [feildsState, setFeildsState] = useState(true);
  const [value, setValue] = useState();
  const [errorState, setErroState] = useState(false);
  const [items, setItems] = useState([]);
  const [selectMenu, setSelectMenu] = useState();
  const companyParam = JSON.parse(localStorage.userData);
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const today = new Date();
  const todayDateStr = moment(today).format("DD-MM-YYYY").toString();
  const todayDateStrForManualRecon = moment(today)
    .format("YYYY-MM-DD")
    .toString();
  const userData = JSON.parse(readLocalStorage("userData"));
  const { reportingKey } = userData;
  const handleCancel = () => {
    setValue(null);
    setErroState(false);
    dispatch(clearAllStates());
    form.resetFields();
    setModalData();
    setTotalSum(0);
    setAcceptedSum(0);
    setButtonState(true);
    setItems([]);
  };

  const countTotal = (notesCount) => {
    let sum = 0;
    Object.entries(notesCount).forEach(([key, value]) => {
      if (key === "_5000") {
        sum += 5000 * value;
      } else if (key === "_1000") {
        sum += 1000 * value;
      } else if (key === "_500") {
        sum += 500 * value;
      } else if (key === "_100") {
        sum += 100 * value;
      } else if (key === "_50") {
        sum += 50 * value;
      } else if (key === "_20") {
        sum += 20 * value;
      } else if (key === "_10") {
        sum += 10 * value;
      }
    });
    setTotalSum(sum);
  };
  const convertDateAndTime = () => {
    const utcTime = moment
      .utc(
        `${modalData?.transDate?.year}-${modalData?.transDate?.month}-${modalData?.transDate?.day}` +
          " " +
          `${modalData?.lastUpdated?.hour}:${modalData?.lastUpdated?.minute}:${modalData?.lastUpdated?.second}`
      )
      .format("YYYY-MM-DD HH:mm:ss");
    let localTime = moment.utc(utcTime).toDate();
    localTime = moment(localTime).format("HH:mm:ss");
    return localTime;
  };
  const onFinish = (values) => {
    setErroState(false);
    setItems([]);
    values.sum = totalSum;
    dispatch(
      postReconcilationDetails({
        denomination: values,
        riderId: modalData && modalData.id,
        date: todayDateStrForManualRecon,
        lastChangedByEmail: companyParam.emailAdmin,
      })
    );
  };

  useEffect(() => {
    if (
      reconcilationDetailsUpdateStatus &&
      reconcilationDetailsUpdateStatus.manualReconcilationDetails
    ) {
      const { manualReconcilationDetails } = reconcilationDetailsUpdateStatus;
      if (manualReconcilationDetails === 200) {
        setValue(null);
        setFeildsState(true);
        dispatch(clearAllStates());
        dispatch(transactionDetailsThunk({ date: todayDateStr }));
        dispatch(getTotalCollection({ date: todayDateStr }));
        dispatch(getInactiveActiveRidersList({ date: todayDateStr }));
        form.resetFields();
        setModalData();
        setTotalSum(0);
        setAcceptedSum(0);
        setButtonState(true);
        setItems([]);
        dispatch(getDeno({ date: todayDateStr }));
      } else if (manualReconcilationDetails === 500) {
        setFeildsState(true);
        setErroState(true);
        setValue(null);
      }
    }
  }, [reconcilationDetailsUpdateStatus]);
  const obj = [
    {
      type: "integer",
      message: "Only numbers allowed",
    },
    {
      pattern: /^[0-9]{0,4}$/,
      message: "Limit Exceed",
    },
  ];
  const compareNotes = (originalNotes, changeNotes) => {
    return _.isEqual(originalNotes, changeNotes);
  };
  const handleOnChange = (e, key) => {
    if ((e && e.toString().length <= 4) || e === 0 || e === null) {
      const stateNote = JSON.parse(JSON.stringify(notesCount));
      stateNote[key] = e === null ? 0 : e;

      setNotesCount(stateNote);
      form.setFieldsValue(stateNote);
      countTotal(stateNote);
      originalNumberOfNotes.noOfOriginalNotes
        ? setButtonState(
            compareNotes(originalNumberOfNotes.noOfOriginalNotes, stateNote)
          )
        : setButtonState(false);
    }
  };
  const handleBackspace = (e, key) => {
    if (e.key === "Backspace") {
      handleOnChange(0, key);
    }
  };

  const onBlur = () => {
    setItems([]);
  };

  const onFocus = () => {};

  const onSearch = (val) => {
    if (val) {
      setTotalSum(0);
      setAcceptedSum(0);
      dispatch(
        searchedRidersList({
          riderId: val,
          companyId: companyParam.companyId,
        })
      );
    }
  };
  const onChange = (value, label) => {
    setValue(label.value);
    form.setFieldsValue({});
    setNotesCount({});
    setTotalSum();
    setFeildsState(false);
    searchRiderFromAggregatedTable(value);
  };

  const searchRiderFromAggregatedTable = (value) => {
    form.resetFields();
    const currValue = value;
    const filteredData = transactionDetailsSelector.transactionDetails.filter(
      (entry) => {
        return entry.riders[reportingKey].includes(currValue);
      }
    );
    if (filteredData.length) {
      const { rejected, accepted } = filteredData[0];
      setModalData(filteredData[0].riders);
      setAcceptedSum(accepted.sum);
      const obj = {
        _5000: 0,
        _1000: 0,
        _500: 0,
        _100: 0,
        _50: 0,
        _20: 0,
        _10: 0,
      };
      let sum = 0;
      obj._5000 = rejected._5000;
      sum += rejected._5000 * 5000;
      obj._1000 = rejected._1000;
      sum += rejected._1000 * 1000;
      obj._500 = rejected._500;
      sum += rejected._500 * 500;
      obj._100 = rejected._100;
      sum += rejected._100 * 100;
      obj._50 = rejected._50;
      sum += rejected._50 * 50;
      obj._20 = rejected._20;
      sum += rejected._20 * 20;
      obj._10 = rejected._10;
      sum += rejected._10 * 10;
      form.setFieldsValue(obj);
      setNotesCount(obj);
      setTotalSum(sum);
      dispatch(getNumberOfNotes(obj));
    } else if (searchRidersStatus) {
      const filteredRider = searchRidersStatus.searchedRiders.filter((entry) =>
        entry.deliveryAgentId.includes(currValue)
      );
      setModalData(filteredRider[0]);
    }
  };
  useEffect(() => {
    if (searchRidersStatus && searchRidersStatus.searchedRiders.length) {
      const items = [];
      for (let i = 0; i < searchRidersStatus.searchedRiders.length; i++) {
        items.push({
          value: searchRidersStatus.searchedRiders[i].deliveryAgentId,
          label: `${searchRidersStatus.searchedRiders[i].deliveryAgentId} ${searchRidersStatus.searchedRiders[i].firstName} ${searchRidersStatus.searchedRiders[i].lastName}`,
        });
      }
      setItems(items);
    }
  }, [searchRidersStatus]);

  return (
    <>
      {" "}
      <Space>
        <Button type="primary" className={stylings.btnStylingMain}>
          Recieve Cash
        </Button>
        <Button type="primary" className={stylings.btnStyling} disabled>
          Transfer Cash
        </Button>
        <Button type="primary" className={stylings.btnStyling} disabled>
          Make Payment
        </Button>
        <Button type="primary" className={stylings.closeBtnStyling} disabled>
          Close
        </Button>
      </Space>
      <div className={stylings.btnBottom}></div>
      <div className={stylings.cashDenoAddBox}>
        <Row style={{ marginBottom: "10px" }}>
          <Col
            xs={24}
            sm={24}
            md={12}
            lg={12}
            xl={12}
            className={stylings.cashDenoBoxLabel}
          >
            <p> Select Account</p>
            <p> Cash Detail </p>
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Select
              showSearch
              style={{ width: "80%" }}
              placeholder="Search Riders"
              onChange={onChange}
              onFocus={onFocus}
              onBlur={onBlur}
              onSearch={onSearch}
              options={items}
              autoFocus={true}
              open={selectMenu}
              onKeyDown={(e) => {
                setSelectMenu();
                if (e.code === "Enter" || e.code === "NumpadEnter") {
                  e.preventDefault();
                  const node = document.getElementById("10");
                  node.focus();
                  setSelectMenu(false);
                }
              }}
              value={value}
            ></Select>
          </Col>
        </Row>

        <div className={stylings.modalRiderInfoBlock}>
          <br />

          <br />
          <span className={stylings.modalRiderInfoText}>
            {modalData?.riderPhoneNumber}
          </span>
          <br />
        </div>

        <div className={stylings.modalTransactionInfoBlock}>
          <span className={stylings.modalTransactionTime}>
            {modalData?.transDate ? convertDateAndTime() : ""}
          </span>
        </div>

        <Form
          form={form}
          name="form_in_modal"
          onFinish={onFinish}
          preserve={false}
        >
          <Row>
            <Col
              xs={24}
              sm={24}
              md={6}
              lg={6}
              xl={6}
              className={stylings.cashDenoBoxLabel}
            >
              <p className={stylings.modalRiderInfoText}>
                {modalData?.firstName && modalData?.lastName ? (
                  <b>
                    {modalData?.firstName} {modalData?.lastName}
                  </b>
                ) : (
                  "Rider Name"
                )}
              </p>

              <p className={stylings.modalRiderInfoText}>
                {modalData?.phoneNumber ? (
                  <b>{modalData?.phoneNumber}</b>
                ) : (
                  "Contact Number"
                )}
              </p>
              <p className={stylings.modalRiderInfoText}>
                {modalData?.cnic ? <b>{modalData?.cnic}</b> : "CNIC"}
              </p>
            </Col>
            <Col xs={24} sm={24} md={18} lg={18} xl={18}>
              <div>
                <Row>
                  <Col xs={24} sm={6} md={6} lg={6} xl={6}>
                    <span className={stylings.denoLabl}>10</span>
                    <Form.Item
                      //   label="10"
                      name="_10"
                      rules={[...obj]}
                    >
                      <InputNumber
                        onChange={(e) => {
                          handleOnChange(e, "_10");
                        }}
                        min={0}
                        // max={9999}
                        onKeyDown={(e) => {
                          handleBackspace(e, "_10");
                        }}
                        disabled={feildsState}
                        style={{
                          width: 70,
                          height: 40,
                        }}
                        id="10"
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={6} md={6} lg={6} xl={6}>
                    <span className={stylings.denoLabl}>20</span>
                    <Form.Item name="_20" rules={[...obj]}>
                      <InputNumber
                        onChange={(e) => {
                          handleOnChange(e, "_20");
                        }}
                        onKeyDown={(e) => {
                          handleBackspace(e, "_20");
                        }}
                        min={0}
                        disabled={feildsState}
                        style={{
                          width: 70,
                          height: 40,
                        }}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={6} md={6} lg={6} xl={6}>
                    <span className={stylings.denoLabl}>50</span>
                    <Form.Item name="_50" rules={[...obj]}>
                      <InputNumber
                        onChange={(e) => {
                          handleOnChange(e, "_50");
                        }}
                        min={0}
                        disabled={feildsState}
                        // max={9999}
                        onKeyDown={(e) => {
                          handleBackspace(e, "_50");
                        }}
                        style={{
                          width: 70,
                          height: 40,
                        }}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={6} md={6} lg={6} xl={6}>
                    <span className={stylings.denoLabl}>100</span>
                    <Form.Item name="_100" rules={[...obj]}>
                      <InputNumber
                        onChange={(e) => {
                          handleOnChange(e, "_100");
                        }}
                        onKeyDown={(e) => {
                          handleBackspace(e, "_100");
                        }}
                        min={0}
                        disabled={feildsState}
                        style={{
                          width: 70,
                          height: 40,
                        }}
                      />
                    </Form.Item>
                  </Col>
                </Row>
              </div>

              <div>
                <Row>
                  <Col xs={24} sm={6} md={6} lg={6} xl={6}>
                    <span className={stylings.denoLabl}>500</span>
                    <Form.Item name="_500" rules={[...obj]}>
                      <InputNumber
                        onChange={(e) => {
                          handleOnChange(e, "_500");
                        }}
                        onKeyDown={(e) => {
                          handleBackspace(e, "_500");
                        }}
                        min={0}
                        disabled={feildsState}
                        style={{
                          width: 70,
                          height: 40,
                        }}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={6} md={6} lg={6} xl={6}>
                    <span className={stylings.denoLabl}>1000</span>
                    <Form.Item name="_1000" rules={[...obj]}>
                      <InputNumber
                        onChange={(e) => {
                          handleOnChange(e, "_1000");
                        }}
                        onKeyDown={(e) => {
                          handleBackspace(e, "_1000");
                        }}
                        min={0}
                        disabled={feildsState}
                        style={{
                          width: 70,
                          height: 40,
                        }}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={6} md={6} lg={6} xl={6}>
                    <span className={stylings.denoLabl}>5000</span>
                    <Form.Item name="_5000" rules={[...obj]}>
                      <InputNumber
                        onChange={(e) => {
                          handleOnChange(e, "_5000");
                        }}
                        onKeyDown={(e) => {
                          handleBackspace(e, "_5000");
                        }}
                        min={0}
                        disabled={feildsState}
                        style={{
                          width: 70,
                          height: 40,
                        }}
                      />
                    </Form.Item>
                  </Col>
                  <Col xs={24} sm={6} md={6} lg={6} xl={6}>
                    <span className={stylings.denoCash}>
                      {totalSum ? numberFormatter(totalSum || 0) : ""}
                    </span>
                  </Col>
                </Row>
              </div>
            </Col>
          </Row>
          <Form.Item>
            <div className={stylings.lessDivider}>
              <p className={stylings.lessDividerText}>Less</p>
            </div>
          </Form.Item>

          <Row>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <Checkbox defaultChecked={true} style={{ marginBottom: "5px" }}>
                Returns
              </Checkbox>
              <br />
              <Checkbox style={{ marginBottom: "5px" }}>Expense</Checkbox>{" "}
              <br />
              <Checkbox style={{ marginBottom: "5px" }}>Discount</Checkbox>{" "}
              <br />
              <Checkbox style={{ marginBottom: "5px" }}>Waiver</Checkbox>
            </Col>
            <Col xs={24} sm={12} md={12} lg={12} xl={12}>
              <p style={{ width: "80%" }}>
                <span className={stylings.bottomSectionText}>In Machine</span>
                <span className={stylings.bottomSectionCash}>
                  {" "}
                  {acceptedSum ? numberFormatter(acceptedSum || 0) : ""}
                </span>
              </p>
              <p style={{ width: "80%" }}>
                <span className={stylings.bottomSectionText}>
                  Total Recieving
                </span>
                <span className={stylings.bottomSectionCash}>
                  {" "}
                  {acceptedSum
                    ? numberFormatter(acceptedSum + totalSum || 0)
                    : ""}
                </span>
              </p>
              <Form.Item style={{ marginBottom: " 5px !important" }}>
                <Space>
                  <Button
                    type="primary"
                    htmlType="submit"
                    disabled={buttonState}
                    size={"large"}
                  >
                    Submit{" "}
                  </Button>
                  <Button
                    size={"large"}
                    onClick={() => {
                      handleCancel();
                    }}
                  >
                    Cancel
                  </Button>
                </Space>
                {errorState ? (
                  <p style={{ fontSize: "small", color: "red" }}>
                    Some went wrong. Please try again
                  </p>
                ) : null}
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    </>
  );
}
