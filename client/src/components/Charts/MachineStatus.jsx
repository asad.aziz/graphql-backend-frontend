import React from "react";
import { Row, Col, Divider } from "antd";
import { Liquid, measureTextWidth } from "@ant-design/charts";
// import dayjs from "dayjs";

import styles from "./style.module.css";
const MachineStatus = () => {
  // const LoadTime = dayjs().format("dddd, MMMM DD, YYYY HH:mm").toString();

  const config = {
    height: 150,
    width: 150,
    percent: 0.75,
    autoFit: true,
    outline: {
      border: 4,
      distance: 8,
    },
    wave: { length: 128 },
    statistic: {
      title: {
        formatter: ({ percent }) => {
          // const text = ` ${100 - (percent * 100).toFixed(0)}%`;
          return "N/A";
        },
        style: ({ percent }) => ({
          fill: percent > 0.65 ? "white" : "rgba(44,53,66,0.85)",
        }),
      },
      content: {
        style: ({ percent }) => ({
          fontSize: 60,
          lineHeight: 1,
          fill: percent > 0.65 ? "white" : "rgba(44,53,66,0.85)",
        }),
        customHtml: (container, view, { percent }) => {
          const { width, height } = container.getBoundingClientRect();
          const d = Math.sqrt(Math.pow(width / 2, 2) + Math.pow(height / 2, 2));
          const text = ` ${(percent * 100).toFixed(0)}%`;
          const textWidth = measureTextWidth(text, { fontSize: 200 });
          const scale = Math.min(d / textWidth, 1);
          return `<div style="width:${d}px;display:flex;align-items:top;justify-content:top;font-size:${scale}em;line-height:${
            scale <= 1 ? 1 : "inherit"
          }">Capacity Available</div>`;
        },
      },
    },
    liquidStyle: ({ percent }) => {
      return {
        fill: percent > 0.45 ? "#9294941" : "#c0c2c2",
        stroke: percent > 0.45 ? "#9294941" : "#c0c2c2",
      };
    },
  };
  const config1 = {
    height: 150,
    width: 150,
    percent: 0.35,
    autoFit: true,
    outline: {
      border: 4,
      distance: 8,
    },
    wave: { length: 128 },
    statistic: {
      title: {
        formatter: ({ percent }) => {
          // const text = ` ${100 - (percent * 100).toFixed(0)}%`;
          return "N/A";
        },
        style: ({ percent }) => ({
          fill: percent > 0.65 ? "white" : "rgba(44,53,66,0.85)",
        }),
      },
      content: {
        style: ({ percent }) => ({
          fontSize: 60,
          lineHeight: 1,
          fill: percent > 0.65 ? "white" : "rgba(44,53,66,0.85)",
        }),
        customHtml: (container, view, { percent }) => {
          const { width, height } = container.getBoundingClientRect();
          const d = Math.sqrt(Math.pow(width / 2, 2) + Math.pow(height / 2, 2));
          const text = ` ${(percent * 100).toFixed(0)}%`;
          const textWidth = measureTextWidth(text, {
            fontSize: 200,
            fontColor: "#c0c2c2",
          });
          const scale = Math.min(d / textWidth, 1);
          return `<div style="width:${d}px;display:flex;align-items:top;justify-content:top;font-size:${scale}em;line-height:${
            scale <= 1 ? 1 : "inherit"
          }">Capacity Available</div>`;
        },
      },
    },
    liquidStyle: ({ percent }) => {
      return {
        fill: percent > 0.45 ? "#9294941" : "#c0c2c2",
        stroke: percent > 0.45 ? "#9294941" : "#c0c2c2",
      };
    },
  };
  return (
    <div className={`${styles.mainContainerCashCollection} box-shadow-style`}>
      <Row className={styles.liquidChartRow}>
        <Col xs={12} sm={12} md={11} lg={11} xl={11}>
          {" "}
          <Liquid {...config} />
          <p className={styles.machineStatusUptime}>Uptime: N/A</p>
          <p className={styles.lastUpdatedAt}>
            <span style={{ fontWeight: "bold" }}>Last updated at :</span>
            {"N/A"}
          </p>
        </Col>
        <Col xs={0} sm={0} md={1} lg={1} xl={1}>
          {" "}
          <Divider type="vertical" style={{ height: "100%" }} />
        </Col>
        <Col xs={12} sm={12} md={11} lg={11} xl={11}>
          {" "}
          <Liquid {...config1} />
          <p className={styles.machineStatusUptime}>Uptime: N/A</p>
          <p className={styles.lastUpdatedAt}>
            <span style={{ fontWeight: "bold" }}>Last updated at :</span>
            {"N/A"}
          </p>
        </Col>
      </Row>
    </div>
  );
};

export default MachineStatus;
