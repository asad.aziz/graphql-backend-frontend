import React, { useEffect, useState } from "react";
import { Column } from "@ant-design/charts";
import { useDispatch, useSelector } from "react-redux";
import dayjs from "dayjs";
import PropTypes from "prop-types";
import {
  unitWiseCollectionThunk,
  selectUnitWiseCollection,
  clearState,
} from "../../redux/unitWiseCollectionSlice";
import styles from "./style.module.css";

const UnitwiseCashCollection = (props) => {
  const { officeName } = props;
  const dispatch = useDispatch();
  const unitWiseDetailsSelector = useSelector(selectUnitWiseCollection);
  const todayDateStr = dayjs().format("DD-MM-YYYY").toString();
  const [dataUnitWise, setDataUnitWise] = useState([
    {
      key: "",
      value: 0,
    },
  ]);

  useEffect(() => {
    //   if (unitWiseDetailsSelector.isloader === null) {
    dispatch(clearState());
    dispatch(unitWiseCollectionThunk({ date: todayDateStr }));
    //  }
  }, []);

  useEffect(() => {
    if (officeName !== "") {
      dispatch(clearState());
      dispatch(
        unitWiseCollectionThunk({ date: todayDateStr, officeName: officeName })
      );
    }
  }, [officeName]);

  useEffect(() => {
    const { unitWiseCollection } = unitWiseDetailsSelector;
    if (unitWiseCollection && unitWiseCollection.length) {
      setDataUnitWise(unitWiseCollection);
    } else {
      setDataUnitWise([
        {
          key: "",
          value: 0,
        },
      ]);
    }
  }, [unitWiseDetailsSelector]);

  useEffect(() => {
    if (officeName !== "") {
      dispatch(
        unitWiseCollectionThunk({ date: todayDateStr, officeName: officeName })
      );
    }
  }, [officeName]);

  const config = {
    data: dataUnitWise && dataUnitWise,
    height: 300,
    xField: "key",
    yField: "value",
    seriesField: "key",
    colorField: "key",
    color: ["#3BB4C1", "#048998"],
    legend: false,
  };
  return (
    <div className={`${styles.mainContainerCashUnit} box-shadow-style`}>
      <Column {...config} />
    </div>
  );
};

export default UnitwiseCashCollection;
UnitwiseCashCollection.propTypes = {
  officeName: PropTypes.any,
};
