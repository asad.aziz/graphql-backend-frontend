/* eslint-disable valid-typeof */
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Column } from "@ant-design/charts";
import { Empty } from "antd";
import dayjs from "dayjs";
import PropTypes from "prop-types";
import styles from "./style.module.css";
import { getDeno, selectDeno } from "../../redux/getDenominationsCount";

const DenominationBarChart = (props) => {
  const { officeName } = props;
  const dispatch = useDispatch();
  const getCashDenoCount = useSelector(selectDeno);
  const [cashDeno, setCashDeno] = useState([]);
  const todayDateStr = dayjs().format("DD-MM-YYYY").toString();
  useEffect(() => {
    // if (getCashDenoCount.isloader === null) {
    dispatch(getDeno({ date: todayDateStr }));
    // }
  }, []);

  useEffect(() => {
    if (officeName !== "") {
      dispatch(getDeno({ date: todayDateStr, officeName: officeName }));
    }
  }, [officeName]);

  useEffect(() => {
    const { denominationsList } = getCashDenoCount;
    const dataForBarChart = [];
    if (denominationsList.length) {
      for (let i = 0; i < denominationsList.length; i++) {
        const currrentIndex = denominationsList[i];
        if (currrentIndex.isManual) {
          Object.entries(currrentIndex).forEach(([key, value]) => {
            if (key !== "isManual") {
              dataForBarChart.push({
                deno: key.substring(1),
                count: value,
                type: "In hand",
              });
            }
          });
        } else if (currrentIndex.isManual !== true) {
          Object.entries(currrentIndex).forEach(([key, value]) => {
            if (key !== "isManual") {
              dataForBarChart.push({
                deno: key.substring(1),
                count: value,
                type: "In Machine",
              });
            }
          });
        }
      }
    }
    setCashDeno(dataForBarChart);
  }, [getCashDenoCount]);

  const config = {
    data: cashDeno,
    height: 300,
    isStack: true,
    xField: "deno", // should be deno
    yField: "count", // should be count
    seriesField: "type",
    legend: {
      layout: "horizontal",
      position: "top-right",
    },
    color: ["#3BB4C1", "#048998"],
  };

  return (
    <div className={`${styles.mainContainerDeno} box-shadow-style`}>
      {cashDeno.length ? (
        <Column {...config} />
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      )}
    </div>
  );
};

export default DenominationBarChart;
DenominationBarChart.propTypes = {
  officeName: PropTypes.any,
};
