/* eslint-disable react/prop-types */
/* eslint-disable react/display-name */
import React, { useEffect, useState } from "react";
import { Table, Input, Popover } from "antd";
import { useDispatch, useSelector } from "react-redux";
import dayjs from "dayjs";
import PropTypes from "prop-types";
import { readLocalStorage } from "../../utils/readLocalStorage";
import { numberFormatter } from "../../utils/numberFormatter";
import {
  selectTransactionDetails,
  transactionDetailsThunk,
} from "../../redux/transactionDetailsSlice";
import { utcToLocalTimeConverter } from "../../utils/utcToLocalTImeConverter";
import styles from "./style.module.css";
import overviewStyle from "../../screens/OverView/style.module.css";
import { timeStampFormatter } from "../../utils/timeStampFormatter";
import { timeDifference } from "../../utils/timeDifference";
import ExcelGeneratorButton from "../Custom/ExcelGeneratorButton";

function TransactionTable(props) {
  const { officeName } = props;
  const dispatch = useDispatch();
  const transactionDetailsSelector = useSelector(selectTransactionDetails);
  const [dataSource, setDataSource] = useState([]);
  const todayDateStr = dayjs().format("DD-MM-YYYY").toString();
  const userData = JSON.parse(readLocalStorage("userData"));
  const { reportingKey } = userData;
  useEffect(() => {
    if (transactionDetailsSelector.loading === null) {
      dispatch(transactionDetailsThunk({ date: todayDateStr }));
    }
  }, []);

  // useEffect(() => {
  //   console.log("datasource ", dataSource);
  // }, [dataSource]);

  useEffect(() => {
    if (officeName !== "") {
      dispatch(
        transactionDetailsThunk({ date: todayDateStr, officeName: officeName })
      );
    }
  }, [officeName]);
  const columns = [
    {
      title: "Notes",
      dataIndex: "notes",
      key: "notes",
      align: "center",
    },
    {
      title: "Count",
      dataIndex: "notesCount",
      key: "notesCount",
      align: "center",
    },
    {
      title: "Total",
      dataIndex: "totalNotesSum",
      key: "totalNotesSum",
      align: "center",
      render: (text, row, index) => {
        return <>{numberFormatter(row.totalNotesSum)}</>;
      },
    },
  ];
  const content = (props) => {
    const { sum, acceptedDenomination } = props;
    const acceptedDenominationArray = [
      {
        notes: "5000",
        notesCount: acceptedDenomination._5000 ? acceptedDenomination._5000 : 0,
        totalNotesSum: acceptedDenomination._5000
          ? acceptedDenomination._5000 * 5000
          : 0,
      },
      {
        notes: "1000",
        notesCount: acceptedDenomination._1000 ? acceptedDenomination._1000 : 0,
        totalNotesSum: acceptedDenomination._1000
          ? acceptedDenomination._1000 * 1000
          : 0,
      },
      {
        notes: "500",
        notesCount: acceptedDenomination._500 ? acceptedDenomination._500 : 0,
        totalNotesSum: acceptedDenomination._500
          ? acceptedDenomination._500 * 500
          : 0,
      },
      {
        notes: "100",
        notesCount: acceptedDenomination._100 ? acceptedDenomination._100 : 0,
        totalNotesSum: acceptedDenomination._100
          ? acceptedDenomination._100 * 100
          : 0,
      },
      {
        notes: "50",
        notesCount: acceptedDenomination._50 ? acceptedDenomination._50 : 0,
        totalNotesSum: acceptedDenomination._50
          ? acceptedDenomination._50 * 50
          : 0,
      },
      {
        notes: "20",
        notesCount: acceptedDenomination._20 ? acceptedDenomination._20 : 0,
        totalNotesSum: acceptedDenomination._20
          ? acceptedDenomination._20 * 20
          : 0,
      },
      {
        notes: "10",
        notesCount: acceptedDenomination._10 ? acceptedDenomination._10 : 0,
        totalNotesSum: acceptedDenomination._10
          ? acceptedDenomination._10 * 10
          : 0,
      },
    ];
    return (
      <>
        <div>
          <Table
            columns={columns}
            dataSource={acceptedDenominationArray}
            pagination={false}
            bordered
            size="small"
            summary={(pageData) => {
              return (
                <>
                  <Table.Summary.Row></Table.Summary.Row>
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={2} align={"right"}>
                      <b>Total</b>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell align={"center"}>
                      {numberFormatter(sum)}
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
          />
        </div>
      </>
    );
  };
  const contentRejected = (props) => {
    const { rejectedDenomination, sum } = props;

    const rejectedDenominationArray = [
      {
        notes: "5000",
        notesCount: rejectedDenomination._5000 ? rejectedDenomination._5000 : 0,
        totalNotesSum: rejectedDenomination._5000
          ? rejectedDenomination._5000 * 5000
          : 0,
      },
      {
        notes: "1000",
        notesCount: rejectedDenomination._1000 ? rejectedDenomination._1000 : 0,
        totalNotesSum: rejectedDenomination._1000
          ? rejectedDenomination._1000 * 1000
          : 0,
      },
      {
        notes: "500",
        notesCount: rejectedDenomination._500 ? rejectedDenomination._500 : 0,
        totalNotesSum: rejectedDenomination._500
          ? rejectedDenomination._500 * 500
          : 0,
      },
      {
        notes: "100",
        notesCount: rejectedDenomination._100 ? rejectedDenomination._100 : 0,
        totalNotesSum: rejectedDenomination._100
          ? rejectedDenomination._100 * 100
          : 0,
      },
      {
        notes: "50",
        notesCount: rejectedDenomination._50 ? rejectedDenomination._50 : 0,
        totalNotesSum: rejectedDenomination._50
          ? rejectedDenomination._50 * 50
          : 0,
      },
      {
        notes: "20",
        notesCount: rejectedDenomination._20 ? rejectedDenomination._20 : 0,
        totalNotesSum: rejectedDenomination._20
          ? rejectedDenomination._20 * 20
          : 0,
      },
      {
        notes: "10",
        notesCount: rejectedDenomination._10 ? rejectedDenomination._10 : 0,
        totalNotesSum: rejectedDenomination._10
          ? rejectedDenomination._10 * 10
          : 0,
      },
    ];
    return (
      <>
        <div>
          <Table
            columns={columns}
            dataSource={rejectedDenominationArray}
            pagination={false}
            bordered
            size="small"
            summary={(pageData) => {
              return (
                <>
                  <Table.Summary.Row></Table.Summary.Row>
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={2} align={"right"}>
                      <b>Total</b>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell align={"center"}>
                      {numberFormatter(sum)}
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
          />
        </div>
      </>
    );
  };

  const contentTotal = (props) => {
    const { totalDenomination, sum } = props;

    const totalDenominationArray = [
      {
        notes: "5000",
        notesCount: totalDenomination._5000 ? totalDenomination._5000 : 0,
        totalNotesSum: totalDenomination._5000
          ? totalDenomination._5000 * 5000
          : 0,
      },
      {
        notes: "1000",
        notesCount: totalDenomination._1000 ? totalDenomination._1000 : 0,
        totalNotesSum: totalDenomination._1000
          ? totalDenomination._1000 * 1000
          : 0,
      },
      {
        notes: "500",
        notesCount: totalDenomination._500 ? totalDenomination._500 : 0,
        totalNotesSum: totalDenomination._500
          ? totalDenomination._500 * 500
          : 0,
      },
      {
        notes: "100",
        notesCount: totalDenomination._100 ? totalDenomination._100 : 0,
        totalNotesSum: totalDenomination._100
          ? totalDenomination._100 * 100
          : 0,
      },
      {
        notes: "50",
        notesCount: totalDenomination._50 ? totalDenomination._50 : 0,
        totalNotesSum: totalDenomination._50 ? totalDenomination._50 * 50 : 0,
      },
      {
        notes: "20",
        notesCount: totalDenomination._20 ? totalDenomination._20 : 0,
        totalNotesSum: totalDenomination._20 ? totalDenomination._20 * 20 : 0,
      },
      {
        notes: "10",
        notesCount: totalDenomination._10 ? totalDenomination._10 : 0,
        totalNotesSum: totalDenomination._10 ? totalDenomination._10 * 10 : 0,
      },
    ];
    return (
      <>
        <div>
          <Table
            columns={columns}
            dataSource={totalDenominationArray}
            pagination={false}
            bordered
            size="small"
            summary={(pageData) => {
              return (
                <>
                  <Table.Summary.Row></Table.Summary.Row>
                  <Table.Summary.Row>
                    <Table.Summary.Cell colSpan={2} align={"right"}>
                      <b>Total</b>
                    </Table.Summary.Cell>
                    <Table.Summary.Cell align={"center"}>
                      {/* {pageData.reduce(
                        (total, obj) => obj.totalNotesSum + total,
                        0
                      )} */}
                      {numberFormatter(sum)}
                    </Table.Summary.Cell>
                  </Table.Summary.Row>
                </>
              );
            }}
          />
        </div>
      </>
    );
  };

  const transactionTableColumns = [
    {
      title: "Rider ID",
      dataIndex: ["riders", `${reportingKey}`],
      width: 5,
      // idToTest.replace(/(\d{5})(\d{7})/g, '$1-$2-')
      render: (text, row, index) => {
        if (reportingKey === "cnic") {
          return text.replace(/(\d{5})(\d{7})/g, "$1-$2-");
        } else if (reportingKey === "phone") {
          return text.replace(/(\d{4})(\d{3})/g, "$1-$2-");
        } else {
          return text;
        }
      },
    },
    {
      title: "First Name",
      dataIndex: ["riders", "firstName"],
      width: 5,
      className: `${styles.tabHeader}`,
    },
    {
      title: "Last Name",
      dataIndex: ["riders", "lastName"],
      width: 5,
    },

    {
      title: "Cash In Machine",
      dataIndex: ["accepted", "sum"],
      width: 5,
      align: "center",
      render: (text, row, index) => {
        return (
          <>
            {" "}
            <Popover
              content={content({
                acceptedDenomination: row.accepted,
                sum: row.accepted.sum,
              })}
              title="Denomination"
              trigger="hover"
              key={"blue"}
            >
              {numberFormatter(text)}
            </Popover>{" "}
          </>
        );
      },
    },
    {
      title: "Cash In Hand",
      dataIndex: ["rejected", "sum"],
      width: 5,
      align: "center",
      render: (text, row, index) => {
        return (
          <>
            {" "}
            <Popover
              content={contentRejected({
                rejectedDenomination: row.rejected,
                sum: row.rejected.sum,
              })}
              title="Denomination"
              trigger="hover"
              key={"blue"}
            >
              {numberFormatter(text)}
            </Popover>{" "}
          </>
        );
      },
    },
    {
      title: "Total",
      dataIndex: ["totalCounts", "sum"],
      width: 5,
      align: "center",
      render: (text, row, index) => {
        return (
          <>
            {" "}
            <Popover
              content={contentTotal({
                totalDenomination: row.totalCounts,
                sum: row.totalCounts.sum,
              })}
              title="Denomination"
              trigger="hover"
              key={"blue"}
            >
              {numberFormatter(text)}
            </Popover>{" "}
          </>
        );
      },
    },
    {
      title: "Last Transaction Time",
      dataIndex: ["accepted", "lastUpdated"],
      width: 5,
      align: "center",
      defaultSortOrder: "descend",
      render: (text, row, index) => {
        let localtime;
        row.accepted.transDate !== undefined
          ? (localtime = utcToLocalTimeConverter(
              row.accepted.transDate,
              row.accepted.lastUpdated
            ))
          : (localtime = utcToLocalTimeConverter(
              row.rejected.transDate,
              row.rejected.lastUpdated
            ));
        return localtime;
      },
      sorter: (a, b) => {
        let aLocal, bLocal;
        a.accepted.transDate !== undefined
          ? (aLocal = timeStampFormatter(
              a.accepted.transDate,
              a.accepted.lastUpdated
            ))
          : (aLocal = timeStampFormatter(
              a.rejected.transDate,
              a.rejected.lastUpdated
            ));

        b.accepted.transDate !== undefined
          ? (bLocal = timeStampFormatter(
              b.accepted.transDate,
              b.accepted.lastUpdated
            ))
          : (bLocal = timeStampFormatter(
              b.rejected.transDate,
              b.rejected.lastUpdated
            ));

        return timeDifference(aLocal, bLocal);
      },
    },
  ];

  const onSearch = (e) => {
    const currValue = e.target.value;
    const filteredData = transactionDetailsSelector.transactionDetails.filter(
      (entry) => entry.riders[reportingKey].includes(currValue)
    );
    setDataSource(filteredData);
  };

  useEffect(() => {
    if (
      transactionDetailsSelector &&
      transactionDetailsSelector.transactionDetails
    ) {
      setDataSource(transactionDetailsSelector.transactionDetails);
    }
  }, [transactionDetailsSelector]);
  return (
    <>
      <p className={overviewStyle.subContainerCardHeading1}>
        Rider transactions - Total Active Riders ({dataSource.length})
      </p>
      <div className={`box-shadow-style`} style={{ background: "white" }}>
        <Input
          placeholder={`Search rider by ${reportingKey}`}
          allowClear
          //  onSearch={onSearch}
          onChange={onSearch}
          style={{ marginBottom: "10px" }}
        />
        <Table
          className={styles.transactionTable}
          columns={transactionTableColumns}
          dataSource={dataSource}
          size="small"
        />
        <ExcelGeneratorButton
          {...{
            excelData: dataSource,
          }}
        />
      </div>
    </>
  );
}

export default TransactionTable;
TransactionTable.propTypes = {
  officeName: PropTypes.any,
};
