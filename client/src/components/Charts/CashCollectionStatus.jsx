import React from "react";
import { Row, Col } from "antd";
import { RingProgress } from "@ant-design/charts";
import styles from "./style.module.css";
const CashCollectionStatus = () => {
  const config = {
    height: 120,
    width: 120,
    autoFit: false,
    percent: 0,
    // color: ["#5B8FF9", "#E8EDF3"],
    color: [("#99c2c2", "#c0c2c2")],
    innerRadius: 0.85,
    radius: 0.98,
    statistic: {
      title: {
        style: {
          color: "#363636",
          fontSize: "12px",
          lineHeight: "14px",
        },
        formatter: function formatter(datum) {
          return "Riders Completed ";
        },
      },
      content: {
        style: { color: "#363636", fontSize: "13px", lineHeight: "20px" },
        formatter: (per) => {
          const { percent } = per;
          console.log(percent);
          return percent > 0 ? percent : "N/A";
        },
      },
    },
  };
  const config1 = {
    height: 120,
    width: 120,
    autoFit: false,
    // percent: 0.6,
    percent: null,
    color: ["#5B8FF9", "#c0c2c2"],
    innerRadius: 0.85,
    radius: 0.98,
    statistic: {
      title: {
        style: {
          color: "#363636",
          fontSize: "12px",
          lineHeight: "14px",
        },
        formatter: function formatter() {
          return "Cash Collected";
        },
      },
      content: {
        style: { color: "#363636", fontSize: "13px", lineHeight: "20px" },
        formatter: (per) => {
          const { percent } = per;
          console.log(percent);
          return percent > 0 ? percent : "N/A";
        },
      },
    },
  };
  return (
    <div className={`${styles.mainContainerCashCollection} box-shadow-style`}>
      <Row className={styles.ringChartRow}>
        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
          {" "}
          <RingProgress {...config} />
        </Col>
        <Col xs={12} sm={12} md={12} lg={12} xl={12}>
          {" "}
          <RingProgress {...config1} />
        </Col>
      </Row>
    </div>
  );
};

export default CashCollectionStatus;
