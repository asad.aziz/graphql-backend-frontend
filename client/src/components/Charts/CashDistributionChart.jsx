import React from "react";
import { Row, Col } from "antd";
import { Pie } from "@ant-design/charts";
import styles from "./style.module.css";
const CashDistributionChart = () => {
  const data = [
    {
      type: "Goods Returned",
      // value: "N/A",
      value: 45,
    },
    {
      type: "Discounts",
      // value: "N/A",
      value: 78323,
    },
    {
      type: "Expensed",
      // value: "N/A",
      value: 3000,
    },
    {
      type: "Damage",
      // value: "N/A",
      value: 6000,
    },
    {
      type: "Waiver",
      // value: "N/A",
      value: 4000,
    },
    {
      type: "Total Shortage",
      // value: "N/A",
      value: 124324,
    },
  ];
  const config = {
    height: 150,
    width: 150,
    appendPadding: 10,
    data: data,
    angleField: "value",
    colorField: "type",
    color: ["#424445", "#6f7070", "#9294940", "#c0c2c2"],
    radius: 1,
    innerRadius: 0.6,
    interactions: [{ type: "element-selected" }, { type: "element-active" }],
    label: false,
    legend: false,
    tooltip: { showContent: false },
    statistic: {
      title: false,
      content: {
        formatter: function formatter() {
          return "";
        },
      },
    },
  };

  return (
    <div className={`${styles.mainContainerCashCollection} box-shadow-style`}>
      <Row className={styles.ringChartRow}>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          {" "}
          <Pie {...config} />
        </Col>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          <Row>
            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
              <ul className={styles.listStyling}>
                <li className={styles.liStyling}>Goods Returned</li>
                <li className={styles.liStyling}>Discounts</li>
                <li className={styles.liStyling}>Expensed</li>
                <li className={styles.liStyling}>Damage</li>
                <li className={styles.liStyling}>Waiver</li>
                <li className={styles.liStyling} style={{ fontWeight: "bold" }}>
                  Total Shortage
                </li>
              </ul>
            </Col>
            <Col xs={12} sm={12} md={12} lg={12} xl={12}>
              <ul className={styles.listStylingNumber}>
                <li className={styles.liStylingNumber}>N/A</li>
                <li className={styles.liStylingNumber}>N/A</li>
                <li className={styles.liStylingNumber}>N/A</li>
                <li className={styles.liStylingNumber}>N/A</li>
                <li className={styles.liStylingNumber}>N/A</li>
                <li
                  className={styles.liStylingNumber}
                  style={{ fontWeight: "bold" }}
                >
                  N/A
                </li>
              </ul>
            </Col>
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default CashDistributionChart;
