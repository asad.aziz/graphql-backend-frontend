/* eslint-disable indent */
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { DualAxes } from "@ant-design/charts";
import { Empty } from "antd";
import dayjs from "dayjs";
import moment from "moment";
import PropTypes from "prop-types";
import ButtonSpinner from "../../components/Loader/ButtonSpinner";
import {
  getVolumeAndTimeTransactions,
  selectVoulmeVsTime,
} from "../../redux/getVolumeVsTimeTransactionsSlice";

import styles from "./style.module.css";

const DualAxesChart = (props) => {
  const { officeName } = props;
  const todayDate = dayjs().format("DD-MM-YYYY").toString();
  const dispatch = useDispatch();
  const getVolumeVsTimeTransactions = useSelector(selectVoulmeVsTime);
  const [data, setData] = useState([]);
  useEffect(() => {
    // console.log("getVolumeVsTimeTransactions", getVolumeVsTimeTransactions);
    //  if (getVolumeVsTimeTransactions.isloader === null) {
    dispatch(getVolumeAndTimeTransactions({ date: todayDate }));
    //  }
  }, []);

  useEffect(() => {
    if (officeName !== "") {
      dispatch(
        getVolumeAndTimeTransactions({
          date: todayDate,
          officeName: officeName,
        })
      );
    }
  }, [officeName]);

  useEffect(() => {
    const { isloader, transactionsList } = getVolumeVsTimeTransactions;
    if (isloader !== true && transactionsList) {
      const alteredArray = [];
      const checkEmptyArray = transactionsList.some(
        (element) => element.sum > 0
      );
      if (checkEmptyArray) {
        for (let i = 0; i < transactionsList.length; i++) {
          alteredArray.push({
            hour: moment
              .utc(transactionsList[i].hour, "HH")
              .local()
              .format("HH"),
            Volume: transactionsList[i].sum,
            Transactions: transactionsList[i].count,
          });
        }
        setData(alteredArray);
      } else {
        setData(alteredArray);
      }
    }
  }, [getVolumeVsTimeTransactions]);
  const config = {
    data: [data, data],
    height: 300,
    xField: "hour",
    yField: ["Volume", "Transactions"],
    yAxis: {
      label: {
        legend: {
          layout: "horizontal",
          position: "right",
        },
      },
    },

    geometryOptions: [
      {
        geometry: "line",
        smooth: true,
        color: "#5B8FF9",
        lineStyle: {
          lineWidth: 4,
        },
      },
      {
        geometry: "line",
        smooth: true,
        color: "#5AD8A6",
        lineStyle: {
          lineWidth: 3,
        },
        point: {
          shape: "circle",
          size: 4,
          style: {
            opacity: 0.5,
            stroke: "#5AD8A6",
            fill: "#fff",
          },
        },
      },
    ],
    animate: {
      appear: {
        animation: "path-in",
        duration: 3000,
      },
    },
  };
  return (
    <div className={`${styles.mainContainerVolVsNum} box-shadow-style`}>
      {getVolumeVsTimeTransactions.isloader ? (
        <ButtonSpinner />
      ) : data.length ? (
        <DualAxes {...config} />
      ) : (
        <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
      )}
    </div>
  );
};

export default DualAxesChart;

DualAxesChart.propTypes = {
  officeName: PropTypes.any,
};
