import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const transactionDetailsSlice = createSlice({
  name: "transactionDetails",
  initialState: {
    loading: null,
    transactionDetails: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.loading = true;
    },
    transactionDetails: (state, action) => {
      state.loading = false;
      state.transactionDetails = action.payload;
    },
  },
});

export const {
  sendRequest,
  transactionDetails,
} = transactionDetailsSlice.actions;

export const transactionDetailsThunk = (props) => (dispatch) => {
  const { date, officeName } = props;
  const jwt = readLocalStorage("jwt");
  let url = "";
  if (officeName === "Overall") {
    url = `${process.env.REACT_APP_EC2_RIDER_AGGREGATED_URL}${date}`;
  } else if (officeName === undefined || officeName === "") {
    url = `${process.env.REACT_APP_EC2_RIDER_AGGREGATED_URL}${date}`;
  } else {
    url = `${process.env.REACT_APP_EC2_RIDER_AGGREGATED_URL}${date}/${officeName}`;
  }
  dispatch(sendRequest());
  axios({
    method: "get",
    url: url,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(transactionDetails(response.data));
    })
    .catch((error) => {
      console.log("error - date:", date);
      console.log(error);
    });
};
export const selectTransactionDetails = (state) =>
  state.transactionDetailsThunk;

export default transactionDetailsSlice.reducer;
