/* eslint-disable no-debugger */
import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const getTotalCollectionSlice = createSlice({
  name: "getTotalCollection",
  initialState: {
    isloader: null,
    totalCollection: null,
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getTotalAmount: (state, action) => {
      state.isloader = false;
      state.totalCollection = action.payload;
    },
    getTotalAmountError: (state, action) => {
      state.isloader = false;
      state.totalCollection = null;
    },
  },
});

export const {
  sendRequest,
  getTotalAmount,
  getTotalAmountError,
} = getTotalCollectionSlice.actions;
export const getTotalCollection = (props) => (dispatch) => {
  const { date, officeName } = props;
  const jwt = readLocalStorage("jwt");
  let url = "";
  if (officeName === "Overall") {
    url = `${process.env.REACT_APP_EC2_TODAY_URL}${date}`;
  } else if (officeName === undefined || officeName === "") {
    url = `${process.env.REACT_APP_EC2_TODAY_URL}${date}`;
  } else {
    url = `${process.env.REACT_APP_EC2_TODAY_URL}${date}/${officeName}`;
  }
  dispatch(sendRequest());
  axios({
    method: "get",
    url: url,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getTotalAmount(response.data));
    })
    .catch((error) => {
      dispatch(getTotalAmountError(error.data));
    });
};
export const selectTotalCollection = (state) => state.getTotalCollection;

export default getTotalCollectionSlice.reducer;
