import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const updatePasswordSlice = createSlice({
  name: "updatePassword",
  initialState: {
    isloader: false,
    isUpdated: "",
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    updatePasswordStatus: (state, action) => {
      if (action.payload === 200) {
        state.isUpdated = true;
      } else if (action.payload === 401) {
        state.isUpdated = false;
      }
    },
  },
});

export const {
  sendRequest,
  updatePasswordStatus,
} = updatePasswordSlice.actions;
export const updatePassword = (props) => (dispatch) => {
  const { currentPassword, newPassword } = props;
  const jwt = readLocalStorage("jwt");
  const userDate = JSON.parse(readLocalStorage("userData"));
  axios({
    method: "post",
    url: `${process.env.REACT_APP_EC2_UPDATE_PASSWORD_URL}`,
    data: {
      currentPassword,
      newPassword,
      email: userDate && userDate.emailAdmin,
    },
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(updatePasswordStatus(response.status));
    })
    .catch((error) => {
      console.log(error);
      dispatch(updatePasswordStatus(401));
    });
};
export const selectUpdatePassword = (state) => state.updatePassword;

export default updatePasswordSlice.reducer;
