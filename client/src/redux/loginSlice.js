/* eslint-disable indent */
import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const loginSlice = createSlice({
  name: "login",
  initialState: {
    isloader: false,
    loginDetails: { status: null },
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    loginDetails: (state, action) => {
      state.isloader = true;
      state.loginDetails.status = action.payload;
    },
  },
});

export const { sendRequest, loginDetails } = loginSlice.actions;
export const login = (props) => (dispatch) => {
  console.log("process.env", process.env);
  const { email, password } = props;

  axios({
    method: "post",
    url: `${process.env.REACT_APP_BACKEND_BASE_URL}login`,
    data: { email, password },
  })
    .then((response) => {
      if (response.data) {
        console.log(response.data);
        localStorage.setItem(
          "userData",
          JSON.stringify({
            isAdminLoggedIn: "true",
            firstNameAdmin: response.data.firstNameAdmin,
            lastNameAdmin: response.data.lastNameAdmin,
            companyName: response.data.companyName,
            companyId: response.data.companyId,
            emailAdmin: response.data.emailAdmin,
            isPasswordChangeRequired: response.data.isPasswordChangeRequired,
            locationObjects: response.data.locationObjects,
            reportingKey:
              response.data.tracking_key === "CNIC"
                ? "cnic"
                : response.data.tracking_key === "DELIVERY_AGENT_ID"
                ? "deliveryAgentId"
                : response.data.tracking_key === "PHONE"
                ? "phone"
                : "deliveryAgentId",
          })
        );
        localStorage.setItem("jwt", response.data.jwt);
      }
      dispatch(loginDetails(response.status));
    })
    .catch((error) => {
      dispatch(loginDetails(error.response.status));
    });
};
export const selectLogin = (state) => state.login;

export default loginSlice.reducer;
