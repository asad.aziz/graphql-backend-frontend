import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const getCompanyDetails = createSlice({
  name: "getCompanyLogoUrl",
  initialState: {
    isloader: null,
    companyDetails: "",
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getCompanyDetailsResponse: (state, action) => {
      state.isloader = false;
      state.companyDetails = action.payload;
    },
    getCompanyDetailsResponseError: (state, action) => {
      state.isloader = false;
      state.companyDetails = "";
    },
  },
});

export const {
  sendRequest,
  getCompanyDetailsResponse,
  getCompanyDetailsResponseError,
} = getCompanyDetails.actions;
export const getCompanyLogo = () => (dispatch) => {
  const jwt = readLocalStorage("jwt");
  dispatch(sendRequest());
  axios({
    method: "get",
    url: `${process.env.REACT_APP_EC2_COMPANY_DETAILS}`,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getCompanyDetailsResponse(response.data));
    })
    .catch((error) => {
      dispatch(getCompanyDetailsResponseError(error));
    });
};
export const selectCompanyDetails = (state) => state.getCompanyLogo;

export default getCompanyDetails.reducer;
