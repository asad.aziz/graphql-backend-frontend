import {
  configureStore,
  combineReducers,
  getDefaultMiddleware,
} from "@reduxjs/toolkit";
import loginReducer from "./loginSlice";
import logoutReducer from "./logoutSlice";
import authReducer from "./authSlice";
import InactiveRiderReducer from "./getInactiveRidersSlice";
import RidersListReducer from "./getRidersListSlice";
import getTotalCollectionReducer from "./getTotalCollection";
import refreshTokenReducer from "./refreshTokenSlice";
import getVolumeAndTimeTransactionsReducer from "./getVolumeVsTimeTransactionsSlice";
import updatePasswordReducer from "./updatePasswordSlice";
import getInactiveActiveRidersReducer from "./getInactiveAndActiveRidersSlice";
import getDenoReducer from "./getDenominationsCount";
import getCompanyLogoReducer from "./getCompanyLogo";
import transactionDetailsReducer from "./transactionDetailsSlice";
import unitWiseCollectionReducer from "./unitWiseCollectionSlice";
import postReconcilationDetailsReducer from "./manualReconSlice";
import searchedRidersListReducer from "./searchRiders";
import getNumberOfNotesReducer from "./storeNotesSlice";
const combinedReducer = combineReducers({
  login: loginReducer,
  logout: logoutReducer,
  authThunk: authReducer,
  getInactiveRidersList: InactiveRiderReducer,
  getRidersList: RidersListReducer,
  getTotalCollection: getTotalCollectionReducer,
  refreshTokenThunk: refreshTokenReducer,
  getVolumeAndTimeTransactions: getVolumeAndTimeTransactionsReducer,
  updatePassword: updatePasswordReducer,
  getInactiveActiveRidersList: getInactiveActiveRidersReducer,
  getDeno: getDenoReducer,
  getCompanyLogo: getCompanyLogoReducer,
  transactionDetailsThunk: transactionDetailsReducer,
  unitWiseCollectionThunk: unitWiseCollectionReducer,
  postReconcilationDetails: postReconcilationDetailsReducer,
  searchedRidersList: searchedRidersListReducer,
  getNumberOfNotes: getNumberOfNotesReducer,
});

const rootReducer = (state, action) => {
  if (
    action.type === "logout/clearAllStates" ||
    action.type === "getInactiveRidersList/clearAllStates" ||
    action.type === "refreshTokenThunk/clearAllStates" ||
    action.type === "postReconcilationDetails/clearAllStates"
  ) {
    state = undefined;
  }
  return combinedReducer(state, action);
};

const customizedMiddleware = getDefaultMiddleware({
  serializableCheck: false,
});
export default configureStore({
  reducer: rootReducer,
  middleware: [...getDefaultMiddleware(), ...customizedMiddleware],
});
