import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const getInactiveRiders = createSlice({
  name: "getRidersInactive",
  initialState: {
    isloader: null,
    inActiveriderList: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getRidersInactive: (state, action) => {
      state.isloader = false;
      state.inActiveriderList = action.payload;
    },
    getRidersInactiveError: (state, action) => {
      state.isloader = false;
      state.inActiveriderList = [];
    },
    clearState: (state) => {
      // state.isloader = false;
      // state.inActiveriderList = [];
    },
  },
});

export const {
  sendRequest,
  getRidersInactive,
  clearState,
  getRidersInactiveError,
} = getInactiveRiders.actions;
export const getInactiveRidersList = (props) => (dispatch) => {
  const { date, officeName } = props;
  const jwt = readLocalStorage("jwt");
  let url = "";
  if (officeName === "Overall") {
    url = `${process.env.REACT_APP_EC2_INACTIVE_RIDERS_URL}${date}`;
  } else if (officeName === undefined || officeName === "") {
    url = `${process.env.REACT_APP_EC2_INACTIVE_RIDERS_URL}${date}`;
  } else {
    url = `${process.env.REACT_APP_EC2_INACTIVE_RIDERS_URL}${date}/${officeName}`;
  }
  dispatch(sendRequest());
  axios({
    method: "get",
    url: url,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getRidersInactive(response.data));
    })
    .catch((error) => {
      dispatch(getRidersInactiveError(error.response));
    });
};
export const selectInactiveRiderDetails = (state) =>
  state.getInactiveRidersList;

export default getInactiveRiders.reducer;
