import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const getDenoCount = createSlice({
  name: "getDenoSlice",
  initialState: {
    isloader: null,
    denominationsList: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getDenoDetails: (state, action) => {
      state.isloader = false;
      state.denominationsList = action.payload;
    },
    getDenoError: (state, action) => {
      state.isloader = false;
      state.denominationsList = [];
    },
  },
});

export const {
  sendRequest,
  getDenoDetails,
  getDenoError,
} = getDenoCount.actions;
export const getDeno = (props) => (dispatch) => {
  const { date, officeName } = props;
  const jwt = readLocalStorage("jwt");
  let url = "";
  if (officeName === "Overall") {
    url = `${process.env.REACT_APP_EC2_COMPANY_DENO_COUNT}${date}`;
  } else if (officeName === undefined || officeName === "") {
    url = `${process.env.REACT_APP_EC2_COMPANY_DENO_COUNT}${date}`;
  } else {
    url = `${process.env.REACT_APP_EC2_COMPANY_DENO_COUNT}${date}/${officeName}`;
  }
  dispatch(sendRequest());
  axios({
    method: "get",
    url: url,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getDenoDetails(response.data));
    })
    .catch((error) => {
      dispatch(getDenoError(error));
    });
};
export const selectDeno = (state) => state.getDeno;

export default getDenoCount.reducer;
