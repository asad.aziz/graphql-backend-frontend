import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { readLocalStorage } from "../utils/readLocalStorage";

export const authSlice = createSlice({
  name: "auth",
  initialState: {
    isloader: false,
    status: null,
  },
  reducers: {
    sendRequest: (state) => {
      state.status = false;
    },

    tokenAuthFailedAction: (state) => {
      state.status = "failed";
    },
    tokenAuthSuccessfulAction: (state) => {
      state.status = "success";
    },
  },
});

export const {
  sendRequest,
  tokenAuthFailedAction,
  tokenAuthSuccessfulAction,
} = authSlice.actions;
export const authThunk = () => (dispatch) => {
  const jwt = readLocalStorage("jwt");
  const userData = JSON.parse(readLocalStorage("userData"));
  dispatch(sendRequest());
  axios({
    method: "get",
    url: `${process.env.REACT_APP_EC2_JWT_VERIFICATION}${userData.emailAdmin}`,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      if (response.data === true) {
        dispatch(tokenAuthSuccessfulAction());
      }
    })
    .catch((error) => {
      console.log("error ", error);
      if (error.response.status === 403) {
        dispatch(tokenAuthFailedAction());
      }
    });
};
export const selectAuth = (state) => state.authThunk;

export default authSlice.reducer;
