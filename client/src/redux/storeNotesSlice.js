import { createSlice } from "@reduxjs/toolkit";

export const storeNotesSlice = createSlice({
  name: "storeNotes",
  initialState: {
    noOfOriginalNotes: {},
  },
  reducers: {
    clearStoreNumberOfNotes: (state, action) => {
      state.noOfOriginalNotes = {};
    },
    storeNumberOfNotes: (state, action) => {
      state.noOfOriginalNotes = action.payload;
    },
  },
});

export const {
  storeNumberOfNotes,
  clearStoreNumberOfNotes,
} = storeNotesSlice.actions;
export const getNumberOfNotes = (props) => (dispatch) => {
  dispatch(clearStoreNumberOfNotes());
  dispatch(storeNumberOfNotes(props));
};
export const selectNumberOfNotes = (state) => state.getNumberOfNotes;

export default storeNotesSlice.reducer;
