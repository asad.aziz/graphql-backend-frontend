import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const searchedRidersSlice = createSlice({
  name: "searchRiders",
  initialState: {
    //  isloader: false,
    searchedRiders: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    searchRiders: (state, action) => {
      // state.isloader = false;
      state.searchedRiders = action.payload;
    },
  },
});

export const { sendRequest, searchRiders } = searchedRidersSlice.actions;
export const searchedRidersList = (props) => (dispatch) => {
  const { riderId } = props;
  const jwt = readLocalStorage("jwt");
  dispatch(sendRequest());
  axios({
    method: "get",
    url: `${process.env.REACT_APP_EC2_SEARCHED_RIDERS_URL}${riderId}`,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(searchRiders(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
};
export const selectSearchedRiders = (state) => state.searchedRidersList;

export default searchedRidersSlice.reducer;
