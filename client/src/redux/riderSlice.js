import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const riderSlice = createSlice({
  name: "rider",
  initialState: {
    isloader: false,
    riderDetails: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getriderAggregatedDetails: (state, action) => {
      state.isloader = false;
      state.riderDetails = action.payload;
    },
    clearAllStates: (state) => {
      // From here we can take action only at this "counter" state
      // But, as we have taken care of this particular "logout" action
      // in rootReducer, we can use it to CLEAR the complete Redux Store's state
    },
  },
});

export const {
  sendRequest,
  getriderAggregatedDetails,
  clearAllStates,
} = riderSlice.actions;
export const getRiderDetails = (props) => (dispatch) => {
  const { companyId, date } = props;
  const jwt = readLocalStorage("jwt");
  dispatch(sendRequest());
  axios({
    method: "get",
    url: `${process.env.REACT_APP_EC2_RIDER_AGGREGATED_URL}${companyId}/${date}`,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getriderAggregatedDetails(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
};
export const selectRiderAggregationDetails = (state) => state.getRiderDetails;

export default riderSlice.reducer;
