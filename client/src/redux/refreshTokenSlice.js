import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const refreshTokenSlice = createSlice({
  name: "refreshTokenApi",
  initialState: {
    isLoading: false,
    tokenStatus: "unchanged",
  },
  reducers: {
    sendRequest: (state) => {
      state.isLoading = true;
    },
    refreshTokenDetails: (state, action) => {
      state.isLoading = false;
      state.tokenStatus = action.payload;
    },
    clearAllStates: (state) => {},
  },
});

export const {
  sendRequest,
  refreshTokenDetails,
  clearAllStates,
} = refreshTokenSlice.actions;

export const refreshTokenApi = (props) => (dispatch) => {
  const { jwt } = props;
  axios({
    method: "get",
    url: process.env.REACT_APP_EC2_REFRESH_TOKEN,
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  })
    .then((response) => {
      localStorage.setItem(
        "userData",
        JSON.stringify({
          isAdminLoggedIn: "true",
          firstNameAdmin: response.data.firstNameAdmin,
          lastNameAdmin: response.data.lastNameAdmin,
          companyName: response.data.companyName,
          companyId: response.data.companyId,
          emailAdmin: response.data.emailAdmin,
          isPasswordChangeRequired: response.data.isPasswordChangeRequired,
        })
      );
      localStorage.setItem("jwt", response.data.jwt);
      dispatch(refreshTokenDetails("refreshed"));
    })
    .catch((error) => {
      console.log("refresh token error ", error);
      dispatch(refreshTokenDetails("error"));
    });
};

export const selectRefreshToken = (state) => state.refreshTokenApi;

export default refreshTokenSlice.reducer;
