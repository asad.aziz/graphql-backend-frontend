import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const getVolVsTimeTransaction = createSlice({
  name: "getVolumeVsTimeTransactionsSlice",
  initialState: {
    isloader: null,
    transactionsList: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getTransactionsDetails: (state, action) => {
      state.isloader = false;
      state.transactionsList = action.payload;
    },
    getTransactionsDetailsError: (state, action) => {
      state.isloader = false;
      state.transactionsList = [];
    },
    clearAllStates: (state) => {
      state.isloader = null;
      state.transactionsList = [];
    },
  },
});

export const {
  sendRequest,
  getTransactionsDetails,
  getTransactionsDetailsError,
  clearAllStates,
} = getVolVsTimeTransaction.actions;
export const getVolumeAndTimeTransactions = (props) => (dispatch) => {
  const { officeName, date } = props;
  const jwt = readLocalStorage("jwt");
  let url = "";
  if (officeName === "Overall") {
    url = `${process.env.REACT_APP_EC2_HOUR_URL}${date}`;
  } else if (officeName === undefined || officeName === "") {
    url = `${process.env.REACT_APP_EC2_HOUR_URL}${date}`;
  } else {
    url = `${process.env.REACT_APP_EC2_HOUR_URL}${date}/${officeName}`;
  }
  dispatch(sendRequest());
  axios({
    method: "get",
    url: url,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      if (response.data.length) {
        dispatch(getTransactionsDetails(response.data));
      } else {
        dispatch(clearAllStates());
      }
    })
    .catch((error) => {
      dispatch(getTransactionsDetailsError(error));
    });
};
export const selectVoulmeVsTime = (state) => state.getVolumeAndTimeTransactions;

export default getVolVsTimeTransaction.reducer;
