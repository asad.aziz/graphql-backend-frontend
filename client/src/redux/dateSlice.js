import { createSlice } from "@reduxjs/toolkit";

export const dateSlice = createSlice({
  name: "dateSlice",
  initialState: {
    isLoading: true,
    date: null,
  },
  reducers: {
    dateDetails: (state, action) => {
      state.isLoading = false;
      state.date = action.payload;
    },
  },
});

export const { dateDetails } = dateSlice.actions;

export const selectDateFromStore = (state) => state.dateDetails;

export default dateSlice.reducer;
