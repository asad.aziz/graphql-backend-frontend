import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const logoutSlice = createSlice({
  name: "logout",
  initialState: {
    isloader: false,
    logoutDetails: { status: null },
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    logoutDetails: (state, action) => {
      state.isloader = true;
      state.logoutDetails.status = action.payload;
    },
    clearAllStates: (state) => {
      // From here we can take action only at this "counter" state
      // But, as we have taken care of this particular "logout" action
      // in rootReducer, we can use it to CLEAR the complete Redux Store's state
    },
  },
});

export const {
  sendRequest,
  logoutDetails,
  clearAllStates,
} = logoutSlice.actions;
export const logout = (props) => (dispatch) => {
  const jwt = readLocalStorage("jwt");

  axios({
    method: "post",
    url: `${process.env.REACT_APP_EC2_INSTANCE_URL}loggingOut`,
    headers: {
      Authorization: `Bearer ${jwt}`,
    },
  })
    .then((response) => {
      dispatch(logoutDetails(response.status));
      localStorage.clear();
    })
    .catch((error) => {
      dispatch(logoutDetails(error.response.status));
    });
};
export const selectLogout = (state) => state.logout;

export default logoutSlice.reducer;
