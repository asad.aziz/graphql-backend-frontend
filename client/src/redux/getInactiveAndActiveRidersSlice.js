import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const getRidersInactiveAndActiveSlice = createSlice({
  name: "getRidersInactiveAndActiveSlice",
  initialState: {
    isloader: null,
    activeInActiveriderList: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getRidersInactiveAndActive: (state, action) => {
      state.isloader = false;
      state.activeInActiveriderList = action.payload;
    },
    getRidersInactiveAndActiveError: (state, action) => {
      state.isloader = false;
      state.activeInActiveriderList = [];
    },
    clearState: (state) => {
      // state.isloader = false;
      // state.inActiveriderList = [];
    },
  },
});

export const {
  sendRequest,
  getRidersInactiveAndActive,
  clearState,
  getRidersInactiveAndActiveError,
} = getRidersInactiveAndActiveSlice.actions;
export const getInactiveActiveRidersList = (props) => (dispatch) => {
  const { date, officeName } = props;
  const jwt = readLocalStorage("jwt");
  let url = "";
  if (officeName === "Overall") {
    url = `${process.env.REACT_APP_EC2_RIDER_ACTIVE_INACTIVE}${date}`;
  } else if (officeName === undefined || officeName === "") {
    url = `${process.env.REACT_APP_EC2_RIDER_ACTIVE_INACTIVE}${date}`;
  } else {
    url = `${process.env.REACT_APP_EC2_RIDER_ACTIVE_INACTIVE}${date}/${officeName}`;
  }
  dispatch(sendRequest());
  axios({
    method: "get",
    url: url,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getRidersInactiveAndActive(response.data));
    })
    .catch((error) => {
      dispatch(getRidersInactiveAndActiveError(error.response));
    });
};
export const selectInactiveRiderDetails = (state) =>
  state.getInactiveActiveRidersList;

export default getRidersInactiveAndActiveSlice.reducer;
