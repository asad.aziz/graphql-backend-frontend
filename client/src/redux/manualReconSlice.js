import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const manualReconcilation = createSlice({
  name: "manualReconcilation",
  initialState: {
    isloader: false,
    manualReconcilationDetails: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    updateManualReconcilation: (state, action) => {
      state.isloader = false;
      state.manualReconcilationDetails = action.payload;
    },
    clearAllStates: (state) => {
      // From here we can take action only at this "counter" state
      // But, as we have taken care of this particular "logout" action
      // in rootReducer, we can use it to CLEAR the complete Redux Store's state
    },
  },
});

export const {
  sendRequest,
  updateManualReconcilation,
} = manualReconcilation.actions;
export const postReconcilationDetails = (props) => (dispatch) => {
  const { date, denomination, lastChangedByEmail, riderId } = props;
  const objToSend = {
    date,
    denomination,
    lastChangedByEmail,
    riderId,
  };

  const jwt = readLocalStorage("jwt");
  dispatch(sendRequest());
  axios({
    method: "post",
    url: `${process.env.REACT_APP_EC2_UPDATE_MANUAL_RECONCILATION_URL}`,
    data: objToSend,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(updateManualReconcilation(response.status));
    })
    .catch((error) => {
      dispatch(updateManualReconcilation(error.response.status));
    });
};
export const selectReconcilationDetails = (state) =>
  state.postReconcilationDetails;

export default manualReconcilation.reducer;
