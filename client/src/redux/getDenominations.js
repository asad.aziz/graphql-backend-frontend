import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const getVolVsTimeTransaction = createSlice({
  name: "getDenominationsSlice",
  initialState: {
    isloader: null,
    denominationsList: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getDenominationsDetails: (state, action) => {
      state.isloader = false;
      state.denominationsList = action.payload;
    },
    getTransactionsDetailsError: (state, action) => {
      state.isloader = false;
      state.denominationsList = [];
    },
  },
});

export const {
  sendRequest,
  getDenominationsDetails,
  getTransactionsDetailsError,
} = getVolVsTimeTransaction.actions;
export const getDenominations = (props) => (dispatch) => {
  const jwt = readLocalStorage("jwt");
  dispatch(sendRequest());
  axios({
    method: "get",
    url: `${process.env.REACT_APP_EC2_HOUR_URL}${props.date}`,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getDenominationsDetails(response.data));
    })
    .catch((error) => {
      dispatch(getTransactionsDetailsError(error));
    });
};
export const selectVoulmeVsTime = (state) => state.getDenominations;

export default getVolVsTimeTransaction.reducer;
