/* eslint-disable no-debugger */
import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const unitWiseCollectionSlice = createSlice({
  name: "unitWiseCollectionThunk",
  initialState: {
    isloader: null,
    unitWiseCollection: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getUnitWiseCollection: (state, action) => {
      state.isloader = false;
      state.unitWiseCollection = action.payload;
    },
    clearState: (state) => {
      state.isloader = null;
      state.unitWiseCollection = [];
    },
  },
});

export const { sendRequest, getUnitWiseCollection, clearState } =
  unitWiseCollectionSlice.actions;
export const unitWiseCollectionThunk = (props) => (dispatch) => {
  const { date, officeName } = props;
  const jwt = readLocalStorage("jwt");
  let url = "";
  if (officeName === "Overall") {
    url = `${process.env.REACT_APP_EC2_UNIT_WISE_OFFICE_WISE_COLLECTION}${date}`;
  } else if (officeName === undefined || officeName === "") {
    url = `${process.env.REACT_APP_EC2_UNIT_WISE_OFFICE_WISE_COLLECTION}${date}`;
  } else {
    url = `${process.env.REACT_APP_EC2_UNIT_WISE_OFFICE_WISE_COLLECTION}${date}/${officeName}`;
  }
  dispatch(sendRequest());
  axios({
    method: "get",
    url: url,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getUnitWiseCollection(response.data));
    })
    .catch((error) => {
      dispatch(getUnitWiseCollection(error.data));
    });
};
export const selectUnitWiseCollection = (state) =>
  state.unitWiseCollectionThunk;

export default unitWiseCollectionSlice.reducer;
