import { createSlice } from "@reduxjs/toolkit";
import { readLocalStorage } from "../utils/readLocalStorage";
import axios from "axios";

export const getRidersSlice = createSlice({
  name: "getRiders",
  initialState: {
    //  isloader: false,
    riderList: [],
  },
  reducers: {
    sendRequest: (state) => {
      state.isloader = true;
    },
    getRiders: (state, action) => {
      // state.isloader = false;
      state.riderList = action.payload;
    },
  },
});

export const { sendRequest, getRiders } = getRidersSlice.actions;
export const getRidersList = (props) => (dispatch) => {
  const jwt = readLocalStorage("jwt");
  dispatch(sendRequest());
  axios({
    method: "get",
    url: `${process.env.REACT_APP_EC2_RIDER_URL}`,
    headers: {
      Authorization: `Bearer ${jwt && jwt}`,
    },
  })
    .then((response) => {
      dispatch(getRiders(response.data));
    })
    .catch((error) => {
      console.log(error);
    });
};
export const selectRiderDetails = (state) => state.getRidersList;

export default getRidersSlice.reducer;
