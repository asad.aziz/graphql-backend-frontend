import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Login from "../screens/Login";
import App from "../screens/App";
import unAuthError from "../screens/UnAuthError";
import { ProtectedRoute } from "./protectedRoute";
const Routers = () => {
  return (
    <>
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={() => {
              return <Redirect to="/login" />;
            }}
          />
          <Route exact path="/login" component={Login} />
          <Route exact path="/unauthorize" component={unAuthError} />
          <ProtectedRoute path="/app" component={App} />
        </Switch>
      </Router>
    </>
  );
};
export default Routers;
