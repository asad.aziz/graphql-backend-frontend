import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Loader from "../components/Loader";
import { Route, Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { readLocalStorage } from "../utils/readLocalStorage";
import { selectAuth, authThunk } from "../redux/authSlice";
import { clearAllStates } from "../redux/logoutSlice";
export const ProtectedRoute = ({ component: Component, ...rest }) => {
  const authStatus = useSelector(selectAuth);
  const [authState, setAuthState] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(authThunk());
  }, []);

  useEffect(() => {
    if (readLocalStorage("jwt") === undefined) {
      setAuthState("no jwt");
    } else {
      setAuthState(authStatus.status);
    }
  }, [authStatus]);
  return (
    <Route
      {...rest}
      render={(props) => {
        if (authState === false || authState === null) {
          return <Loader />;
        } else if (authState === "failed") {
          localStorage.clear();
          dispatch(clearAllStates());
          return (
            <Redirect
              to={{
                pathname: "/login",
                state: {
                  from: props.location,
                },
              }}
            />
          );
        } else {
          return <Component {...props} />;
        }
      }}
    />
  );
};

ProtectedRoute.propTypes = {
  component: PropTypes.any,
  location: PropTypes.object,
};
