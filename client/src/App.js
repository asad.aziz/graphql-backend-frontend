import React from "react";
import Router from "./router";
import "./App.less";

const App = () => {
  return <Router />;
};

export default App;
