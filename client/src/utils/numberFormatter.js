export const numberFormatter = (number) => {
  const formatter = new Intl.NumberFormat();
  if (number === 0) {
    return 0;
  } else {
    return formatter.format(number);
  }
};
