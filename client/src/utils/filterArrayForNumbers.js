export const filterArrayForNumbers = (array) => {
  array = array.filter((arr) => {
    return typeof parseInt(arr) === "number" && !isNaN(parseInt(arr));
  });
  return array;
};
