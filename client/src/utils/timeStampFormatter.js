import * as dayjs from "dayjs";
const utc = require("dayjs/plugin/utc");
dayjs.extend(utc);

const objectSupport = require("dayjs/plugin/objectSupport");
dayjs.extend(objectSupport);

const timezone = require("dayjs/plugin/timezone");
dayjs.extend(timezone);

export const timeStampFormatter = (dateObj, timeObj) => {
  const serverDateTime = dayjs.utc({
    year: dateObj.year,
    month: dateObj.month === 0 ? 0 : dateObj.month - 1,
    date: dateObj.day,
    hour: timeObj.hour,
    minute: timeObj.minute,
    second: timeObj.second,
    millisecond: timeObj.nano,
  });

  const localTz = dayjs.tz.guess();
  // console.log("timeStampFormatter");
  // return serverDateTime.tz(localTz).unix();
  return serverDateTime.tz(localTz).format("DD-MM-YYYY hh:mm:ss A");
};
