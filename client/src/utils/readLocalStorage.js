export function readLocalStorage(name) {
  const item = localStorage.getItem(name);
  if (item !== "") {
    return item;
  } else {
    return null;
  }
}
