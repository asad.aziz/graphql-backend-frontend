import moment from "moment";
export const timeDifference = (a, b) => {
  const tStart = moment(a);
  const tEnd = moment(b);
  return moment.duration(tStart.diff(tEnd)).asHours();
};
