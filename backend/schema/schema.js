const graphql = require("graphql");
// const _ = require("lodash");
const axios = require("axios");
const { pool } = require("../db/connection");
const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLSchema,
  GraphQLList,
  GraphQLID,
  GraphQLNonNull,
  GraphQLBoolean,
} = graphql;

const CompanyType = new GraphQLObjectType({
  name: "Company",
  fields: () => ({
    id: {
      type: GraphQLID,
    },
    name: { type: GraphQLString },
    description: { type: GraphQLString },
    users: {
      type: new GraphQLList(UserType),
      resolve(parentValue, args) {
        const { id } = parentValue;
        console.log("companyId==", parentValue);
        return axios
          .get(`http://localhost:3000/companies/${id}/users`)
          .then((res) => res.data);
      },
    },
  }),
});
const UserType = new GraphQLObjectType({
  name: "User",
  fields: () => ({
    id: { type: GraphQLID },
    firstName: { type: GraphQLString },
    age: { type: GraphQLInt },
    company: {
      type: CompanyType,
      resolve(parentValue, args) {
        // console.log(parentValue, args);
        const { companyId } = parentValue;
        return axios
          .get(`http://localhost:3000/companies/${companyId}`)
          .then((res) => res.data);
      },
    },
  }),
});

const RiderType = new GraphQLObjectType({
  // name: "Rider",
  // fields: {
  //   // user: {
  //   //   type: UserType,
  //   //   args: {
  //   //     id: { type: GraphQLString },
  //   //   },
  //   //   resolve(parentValue, args) {
  //   //     // move the resolve function to here
  //   //     return _.find(users, { id: args.id });
  //   //   },
  //   // },
  // },
  name: "Rider",
  //  type: new GraphQLList(RiderType),
  fields: () => ({
    id: { type: GraphQLID },
    cnic: { type: GraphQLString },
    delivery_agent_id: { type: GraphQLString },
    first_name: { type: GraphQLString },
    is_rider_profile_active: { type: GraphQLBoolean },
    last_name: { type: GraphQLString },
    phone_number: { type: GraphQLString },
    company_id: { type: GraphQLString },
    email: { type: GraphQLString },
    slack: { type: GraphQLString },
    office_location: { type: GraphQLString },
    // firstName: { type: GraphQLString },
    // age: { type: GraphQLInt },
    // company: {
    //   type: CompanyType,
    // },
  }),
});

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    user: {
      type: UserType,
      args: { id: { type: GraphQLString } },
      resolve(parentValue, args) {
        // here we go into db
        // or even anyting we want to do
        const { id } = args;
        // console.log(id);
        // return _.find(users, { id });
        return axios
          .get(`http://localhost:3000/users/${id}`)
          .then((resp) => resp.data);
      },
    },
    company: {
      type: CompanyType,
      args: { id: { type: GraphQLString } },
      resolve(parentValue, args) {
        // here we go into db
        // or even anyting we want to do
        const { id } = args;
        // console.log(id);
        // return _.find(users, { id });
        return axios
          .get(`http://localhost:3000/companies/${id}`)
          .then((resp) => resp.data);
      },
    },
    rider: {
      // type: RiderType,
      type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(RiderType))),
      args: { id: { type: GraphQLID } },
      resolve(parentValue, args) {
        // here we go into db
        // or even anyting we want to do
        const { id } = args;
        console.log(id);
        return pool
          .query(
            `SELECT id, cnic, delivery_agent_id, first_name, is_rider_profile_active, last_name, phone_number, company_id, email, slack, office_location FROM public.riders where company_id = '${id}'`
            // (err, res) => {
            //   pool.end();
            // }
          )
          .then((res) => {
            // console.log(res.rows);
            //  pool.end();
            const map1 = res.rows.map((x) => x);
            return map1;
          })
          .catch((e) => console.error(e.stack));
        // client.connect();
        // client.query(
        //   `SELECT id, cnic, delivery_agent_id, first_name, is_rider_profile_active, last_name, phone_number, company_id, email, slack, office_location FROM public.riders where company_id = 'Retailo'`,
        //   (err, res) => {
        //     riders = res && res;
        //     console.log(res);
        //     client.end();
        //   }
        // );
        // // console.log(id);
        // // return _.find(users, { id });
        // client.end();
      },
    },
    // rider: {
    //   type: RiderType,
    //   args: {
    //     id: { type: GraphQLString },
    //   },
    //   resolve(parentValue, args) {
    //     // move the resolve function to here
    //     // return _.find(users, {id: args.id});
    //   },
    // },
  },
});

const mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    addUser: {
      type: UserType,
      args: {
        firstName: { type: new GraphQLNonNull(GraphQLString) },
        age: { type: new GraphQLNonNull(GraphQLInt) },
        companyId: { type: GraphQLString },
      },
      resolve(parentValue, { firstName, age }) {
        return axios
          .post("http://localhost:3000/users", {
            firstName,
            age,
          })
          .then((res) => res.data);
      },
    },
    deleteUser: {
      type: UserType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(parentValue, { id }) {
        return axios
          .delete(`http://localhost:3000/users/${id}`)
          .then((res) => res.data);
      },
    },
    updateUser: {
      type: UserType,
      args: {
        id: { type: GraphQLID },
        firstName: { type: GraphQLString },
        age: { type: GraphQLInt },
        companyId: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(parentValue, args) {
        // console.log("args", args);
        return axios
          .patch(`http://localhost:3000/users/${args.id}`, args)
          .then((res) => res.data);
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation,
});

// client.connect();
// client.query(
//   `SELECT id, cnic, delivery_agent_id, first_name, is_rider_profile_active, last_name, phone_number, company_id, email, slack, office_location FROM public.riders where company_id = 'Retailo'`,
//   (err, res) => {
//     console.log(res.rows);
//     client.end();
//   }
// );
