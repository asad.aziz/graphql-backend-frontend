const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schema/schema");

// const { client } = require("./db/connection");
var cors = require("cors");
const app = express();
app.use(cors());
// const client = new Client({
//   user: "postgres",
//   host: "100.83.123.66:5432",
//   database: "XfoilDataStore",
//   // password: "1234abcd",
//   port: 5432,
// });

// client.connect();

// client.connect();
// client.query(
//   `SELECT id, cnic, delivery_agent_id, first_name, is_rider_profile_active, last_name, phone_number, company_id, email, slack, office_location FROM public.riders where company_id = 'Retailo'`,
//   (err, res) => {
//     console.log(res.rows);
//     client.end();
//   }
// );
// const pgp = require("pg-promise");
// const Promise = require("bluebird");
// const initOptions = {
//   promiseLib: Promise,
//   pgFormatting: true,
// };
// var db = pgp("postgres://100.83.123.66:5432/XfoilDataStore")(initOptions);

// db.one(
//   `SELECT id, cnic, delivery_agent_id, first_name, is_rider_profile_active, last_name, phone_number, company_id, email, slack, office_location
// FROM public.riders`
// )
//   .then(function (data) {
//     console.log("DATA:", data);
//     console.log("DATA:", data.value);
//   })
//   .catch(function (error) {
//     console.log("ERROR:", error);
//   });
app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true,
  })
);

app.listen(4000, () => {
  console.log("Listening");
});
